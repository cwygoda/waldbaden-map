<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis styleCategories="AllStyleCategories" simplifyAlgorithm="0" minScale="100000000" maxScale="0" hasScaleBasedVisibilityFlag="0" simplifyDrawingHints="1" symbologyReferenceScale="-1" simplifyMaxScale="1" readOnly="0" labelsEnabled="0" simplifyLocal="1" version="3.22.1-Białowieża" simplifyDrawingTol="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal startField="" mode="0" startExpression="" durationField="" fixedDuration="0" endField="" endExpression="" limitMode="0" enabled="0" durationUnit="min" accumulate="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 type="RuleRenderer" enableorderby="0" symbollevels="0" referencescale="-1" forceraster="0">
    <rules key="{0004fd96-889a-4292-83e6-37f97fc615b4}">
      <rule key="{365842c0-282f-430b-aaff-845fd7f12017}" symbol="0" checkstate="0"/>
      <rule key="{3f9027f5-d35d-4cbe-a82f-a2492b2c691f}" label="Forest" filter="&quot;landuse&quot; in ('forest')" symbol="1"/>
      <rule key="{b3616a5c-3373-411e-ade2-a8f66adf2dfa}" label="Farmland" filter="&quot;landuse&quot; in ('farmland', 'farmyard', 'meadow', 'allotments', 'grass')" symbol="2"/>
      <rule key="{6befa974-f65e-4290-a680-2fcba887d0c4}" label="Residential" filter="&quot;landuse&quot; in ('residential', 'commercial')" symbol="3"/>
    </rules>
    <symbols>
      <symbol type="fill" force_rhr="0" alpha="1" name="0" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleFill" pass="0" enabled="1">
          <Option type="Map">
            <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
            <Option type="QString" value="229,182,54,255" name="color"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="35,35,35,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.26" name="outline_width"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="solid" name="style"/>
          </Option>
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="229,182,54,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.26" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="fill" force_rhr="0" alpha="1" name="1" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleFill" pass="0" enabled="1">
          <Option type="Map">
            <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
            <Option type="QString" value="43,145,79,255" name="color"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="35,35,35,255" name="outline_color"/>
            <Option type="QString" value="no" name="outline_style"/>
            <Option type="QString" value="0.26" name="outline_width"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="solid" name="style"/>
          </Option>
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="43,145,79,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="no" k="outline_style"/>
          <prop v="0.26" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="fill" force_rhr="0" alpha="1" name="2" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleFill" pass="0" enabled="1">
          <Option type="Map">
            <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
            <Option type="QString" value="165,179,160,255" name="color"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="35,35,35,255" name="outline_color"/>
            <Option type="QString" value="no" name="outline_style"/>
            <Option type="QString" value="0.26" name="outline_width"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="solid" name="style"/>
          </Option>
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="165,179,160,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="no" k="outline_style"/>
          <prop v="0.26" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol type="fill" force_rhr="0" alpha="1" name="3" clip_to_extent="1">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" class="SimpleFill" pass="0" enabled="1">
          <Option type="Map">
            <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
            <Option type="QString" value="237,237,237,255" name="color"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="35,35,35,255" name="outline_color"/>
            <Option type="QString" value="no" name="outline_style"/>
            <Option type="QString" value="0.26" name="outline_width"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="solid" name="style"/>
          </Option>
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="237,237,237,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="no" k="outline_style"/>
          <prop v="0.26" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <customproperties>
    <Option type="Map">
      <Option type="int" value="0" name="embeddedWidgets/count"/>
      <Option type="StringList" name="variableNames">
        <Option type="QString" value="quickosm_query"/>
      </Option>
      <Option type="StringList" name="variableValues">
        <Option type="QString" value="[out:xml] [timeout:25];&#xa;(&#xa;    node[&quot;landuse&quot;]( 52.74119,13.4638,52.78308,13.59963);&#xa;    way[&quot;landuse&quot;]( 52.74119,13.4638,52.78308,13.59963);&#xa;    relation[&quot;landuse&quot;]( 52.74119,13.4638,52.78308,13.59963);&#xa;);&#xa;(._;>;);&#xa;out body;"/>
      </Option>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory sizeType="MM" spacingUnitScale="3x:0,0,0,0,0,0" lineSizeScale="3x:0,0,0,0,0,0" backgroundAlpha="255" rotationOffset="270" spacing="5" barWidth="5" minimumSize="0" direction="0" spacingUnit="MM" penColor="#000000" labelPlacementMethod="XHeight" sizeScale="3x:0,0,0,0,0,0" enabled="0" lineSizeType="MM" penAlpha="255" minScaleDenominator="0" opacity="1" backgroundColor="#ffffff" width="15" penWidth="0" showAxis="1" scaleBasedVisibility="0" scaleDependency="Area" maxScaleDenominator="1e+08" diagramOrientation="Up" height="15">
      <fontProperties style="" description="Cantarell,11,-1,5,50,0,0,0,0,0"/>
      <attribute label="" color="#000000" field=""/>
      <axisSymbol>
        <symbol type="line" force_rhr="0" alpha="1" name="" clip_to_extent="1">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <layer locked="0" class="SimpleLine" pass="0" enabled="1">
            <Option type="Map">
              <Option type="QString" value="0" name="align_dash_pattern"/>
              <Option type="QString" value="square" name="capstyle"/>
              <Option type="QString" value="5;2" name="customdash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
              <Option type="QString" value="MM" name="customdash_unit"/>
              <Option type="QString" value="0" name="dash_pattern_offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
              <Option type="QString" value="0" name="draw_inside_polygon"/>
              <Option type="QString" value="bevel" name="joinstyle"/>
              <Option type="QString" value="35,35,35,255" name="line_color"/>
              <Option type="QString" value="solid" name="line_style"/>
              <Option type="QString" value="0.26" name="line_width"/>
              <Option type="QString" value="MM" name="line_width_unit"/>
              <Option type="QString" value="0" name="offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="offset_unit"/>
              <Option type="QString" value="0" name="ring_filter"/>
              <Option type="QString" value="0" name="trim_distance_end"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_end_unit"/>
              <Option type="QString" value="0" name="trim_distance_start"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_start_unit"/>
              <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
              <Option type="QString" value="0" name="use_custom_dash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
            </Option>
            <prop v="0" k="align_dash_pattern"/>
            <prop v="square" k="capstyle"/>
            <prop v="5;2" k="customdash"/>
            <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
            <prop v="MM" k="customdash_unit"/>
            <prop v="0" k="dash_pattern_offset"/>
            <prop v="3x:0,0,0,0,0,0" k="dash_pattern_offset_map_unit_scale"/>
            <prop v="MM" k="dash_pattern_offset_unit"/>
            <prop v="0" k="draw_inside_polygon"/>
            <prop v="bevel" k="joinstyle"/>
            <prop v="35,35,35,255" k="line_color"/>
            <prop v="solid" k="line_style"/>
            <prop v="0.26" k="line_width"/>
            <prop v="MM" k="line_width_unit"/>
            <prop v="0" k="offset"/>
            <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
            <prop v="MM" k="offset_unit"/>
            <prop v="0" k="ring_filter"/>
            <prop v="0" k="trim_distance_end"/>
            <prop v="3x:0,0,0,0,0,0" k="trim_distance_end_map_unit_scale"/>
            <prop v="MM" k="trim_distance_end_unit"/>
            <prop v="0" k="trim_distance_start"/>
            <prop v="3x:0,0,0,0,0,0" k="trim_distance_start_map_unit_scale"/>
            <prop v="MM" k="trim_distance_start_unit"/>
            <prop v="0" k="tweak_dash_pattern_on_corners"/>
            <prop v="0" k="use_custom_dash"/>
            <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings showAll="1" linePlacementFlags="18" zIndex="0" dist="0" priority="0" placement="1" obstacle="0">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option type="Map" name="QgsGeometryGapCheck">
        <Option type="double" value="0" name="allowedGapsBuffer"/>
        <Option type="bool" value="false" name="allowedGapsEnabled"/>
        <Option type="QString" value="" name="allowedGapsLayer"/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="None" name="full_id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="osm_id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="osm_type">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="landuse">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="meadow">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="basin">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="old_name:1991-1999">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="old_name:1946-1990">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="disused:amenity">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="description:en">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="wetland">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="wikipedia">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="water">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ref">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="leisure">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="description">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="sport">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="natural">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="fee">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="wikidata">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="type">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="leaf_type">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="leaf_cycle">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="full_id"/>
    <alias index="1" name="" field="osm_id"/>
    <alias index="2" name="" field="osm_type"/>
    <alias index="3" name="" field="landuse"/>
    <alias index="4" name="" field="meadow"/>
    <alias index="5" name="" field="basin"/>
    <alias index="6" name="" field="old_name:1991-1999"/>
    <alias index="7" name="" field="old_name:1946-1990"/>
    <alias index="8" name="" field="disused:amenity"/>
    <alias index="9" name="" field="description:en"/>
    <alias index="10" name="" field="wetland"/>
    <alias index="11" name="" field="wikipedia"/>
    <alias index="12" name="" field="water"/>
    <alias index="13" name="" field="ref"/>
    <alias index="14" name="" field="leisure"/>
    <alias index="15" name="" field="description"/>
    <alias index="16" name="" field="sport"/>
    <alias index="17" name="" field="natural"/>
    <alias index="18" name="" field="fee"/>
    <alias index="19" name="" field="wikidata"/>
    <alias index="20" name="" field="name"/>
    <alias index="21" name="" field="type"/>
    <alias index="22" name="" field="leaf_type"/>
    <alias index="23" name="" field="leaf_cycle"/>
  </aliases>
  <defaults>
    <default applyOnUpdate="0" expression="" field="full_id"/>
    <default applyOnUpdate="0" expression="" field="osm_id"/>
    <default applyOnUpdate="0" expression="" field="osm_type"/>
    <default applyOnUpdate="0" expression="" field="landuse"/>
    <default applyOnUpdate="0" expression="" field="meadow"/>
    <default applyOnUpdate="0" expression="" field="basin"/>
    <default applyOnUpdate="0" expression="" field="old_name:1991-1999"/>
    <default applyOnUpdate="0" expression="" field="old_name:1946-1990"/>
    <default applyOnUpdate="0" expression="" field="disused:amenity"/>
    <default applyOnUpdate="0" expression="" field="description:en"/>
    <default applyOnUpdate="0" expression="" field="wetland"/>
    <default applyOnUpdate="0" expression="" field="wikipedia"/>
    <default applyOnUpdate="0" expression="" field="water"/>
    <default applyOnUpdate="0" expression="" field="ref"/>
    <default applyOnUpdate="0" expression="" field="leisure"/>
    <default applyOnUpdate="0" expression="" field="description"/>
    <default applyOnUpdate="0" expression="" field="sport"/>
    <default applyOnUpdate="0" expression="" field="natural"/>
    <default applyOnUpdate="0" expression="" field="fee"/>
    <default applyOnUpdate="0" expression="" field="wikidata"/>
    <default applyOnUpdate="0" expression="" field="name"/>
    <default applyOnUpdate="0" expression="" field="type"/>
    <default applyOnUpdate="0" expression="" field="leaf_type"/>
    <default applyOnUpdate="0" expression="" field="leaf_cycle"/>
  </defaults>
  <constraints>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="full_id"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="osm_id"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="osm_type"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="landuse"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="meadow"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="basin"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="old_name:1991-1999"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="old_name:1946-1990"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="disused:amenity"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="description:en"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="wetland"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="wikipedia"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="water"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="ref"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="leisure"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="description"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="sport"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="natural"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="fee"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="wikidata"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="name"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="type"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="leaf_type"/>
    <constraint exp_strength="0" constraints="0" unique_strength="0" notnull_strength="0" field="leaf_cycle"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="full_id"/>
    <constraint desc="" exp="" field="osm_id"/>
    <constraint desc="" exp="" field="osm_type"/>
    <constraint desc="" exp="" field="landuse"/>
    <constraint desc="" exp="" field="meadow"/>
    <constraint desc="" exp="" field="basin"/>
    <constraint desc="" exp="" field="old_name:1991-1999"/>
    <constraint desc="" exp="" field="old_name:1946-1990"/>
    <constraint desc="" exp="" field="disused:amenity"/>
    <constraint desc="" exp="" field="description:en"/>
    <constraint desc="" exp="" field="wetland"/>
    <constraint desc="" exp="" field="wikipedia"/>
    <constraint desc="" exp="" field="water"/>
    <constraint desc="" exp="" field="ref"/>
    <constraint desc="" exp="" field="leisure"/>
    <constraint desc="" exp="" field="description"/>
    <constraint desc="" exp="" field="sport"/>
    <constraint desc="" exp="" field="natural"/>
    <constraint desc="" exp="" field="fee"/>
    <constraint desc="" exp="" field="wikidata"/>
    <constraint desc="" exp="" field="name"/>
    <constraint desc="" exp="" field="type"/>
    <constraint desc="" exp="" field="leaf_type"/>
    <constraint desc="" exp="" field="leaf_cycle"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
    <actionsetting type="5" notificationMessage="" isEnabledOnlyWhenEditable="0" name="OpenStreetMap Browser" capture="0" shortTitle="OpenStreetMap Browser" action="http://www.openstreetmap.org/browse/[% &quot;osm_type&quot; %]/[% &quot;osm_id&quot; %]" id="{bd8cfe65-2860-4aba-be23-97bc436d79f7}" icon="">
      <actionScope id="Feature"/>
      <actionScope id="Field"/>
      <actionScope id="Canvas"/>
    </actionsetting>
    <actionsetting type="1" notificationMessage="" isEnabledOnlyWhenEditable="0" name="JOSM" capture="0" shortTitle="JOSM" action="from QuickOSM.core.actions import Actions;Actions.run(&quot;josm&quot;,&quot;[% &quot;full_id&quot; %]&quot;)" id="{15b5af24-d394-4da1-9252-f779019683f9}" icon="/home/crischan/.local/share/QGIS/QGIS3/profiles/default/python/plugins/QuickOSM/resources/icons/josm_icon.svg">
      <actionScope id="Feature"/>
      <actionScope id="Field"/>
      <actionScope id="Canvas"/>
    </actionsetting>
    <actionsetting type="5" notificationMessage="" isEnabledOnlyWhenEditable="0" name="User default editor" capture="0" shortTitle="User default editor" action="http://www.openstreetmap.org/edit?[% &quot;osm_type&quot; %]=[% &quot;osm_id&quot; %]" id="{8d443c6d-34b9-4538-8fc9-bb12349b80e5}" icon="">
      <actionScope id="Feature"/>
      <actionScope id="Field"/>
      <actionScope id="Canvas"/>
    </actionsetting>
    <actionsetting type="1" notificationMessage="" isEnabledOnlyWhenEditable="0" name="wikipedia" capture="0" shortTitle="wikipedia" action="from QuickOSM.core.actions import Actions;Actions.run(&quot;wikipedia&quot;,&quot;[% &quot;wikipedia&quot; %]&quot;)" id="{7a0f7503-372a-4fb9-98fa-f2eda5e0eeeb}" icon="/home/crischan/.local/share/QGIS/QGIS3/profiles/default/python/plugins/QuickOSM/resources/icons/wikipedia.png">
      <actionScope id="Feature"/>
      <actionScope id="Field"/>
      <actionScope id="Canvas"/>
    </actionsetting>
    <actionsetting type="1" notificationMessage="" isEnabledOnlyWhenEditable="0" name="wikidata" capture="0" shortTitle="wikidata" action="from QuickOSM.core.actions import Actions;Actions.run(&quot;wikidata&quot;,&quot;[% &quot;wikidata&quot; %]&quot;)" id="{2a5f11cb-0ca7-4092-bc84-0850377e152a}" icon="/home/crischan/.local/share/QGIS/QGIS3/profiles/default/python/plugins/QuickOSM/resources/icons/wikidata.png">
      <actionScope id="Feature"/>
      <actionScope id="Field"/>
      <actionScope id="Canvas"/>
    </actionsetting>
    <actionsetting type="1" notificationMessage="" isEnabledOnlyWhenEditable="0" name="Reload the query in a new file" capture="0" shortTitle="Reload the query in a new file" action="from QuickOSM.core.actions import Actions;Actions.run_reload(layer_name=&quot;landuse&quot;)" id="{702ff7f0-240d-4ac4-8bc5-151377eb1351}" icon="">
      <actionScope id="Layer"/>
    </actionsetting>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortExpression="" sortOrder="0">
    <columns>
      <column type="field" hidden="0" name="full_id" width="-1"/>
      <column type="field" hidden="0" name="osm_id" width="-1"/>
      <column type="field" hidden="0" name="osm_type" width="-1"/>
      <column type="field" hidden="0" name="landuse" width="-1"/>
      <column type="field" hidden="0" name="meadow" width="-1"/>
      <column type="field" hidden="0" name="basin" width="-1"/>
      <column type="field" hidden="0" name="old_name:1991-1999" width="-1"/>
      <column type="field" hidden="0" name="old_name:1946-1990" width="-1"/>
      <column type="field" hidden="0" name="disused:amenity" width="-1"/>
      <column type="field" hidden="0" name="description:en" width="-1"/>
      <column type="field" hidden="0" name="wetland" width="-1"/>
      <column type="field" hidden="0" name="wikipedia" width="-1"/>
      <column type="field" hidden="0" name="water" width="-1"/>
      <column type="field" hidden="0" name="ref" width="-1"/>
      <column type="field" hidden="0" name="leisure" width="-1"/>
      <column type="field" hidden="0" name="description" width="-1"/>
      <column type="field" hidden="0" name="sport" width="-1"/>
      <column type="field" hidden="0" name="natural" width="-1"/>
      <column type="field" hidden="0" name="fee" width="-1"/>
      <column type="field" hidden="0" name="wikidata" width="-1"/>
      <column type="field" hidden="0" name="name" width="-1"/>
      <column type="field" hidden="0" name="type" width="-1"/>
      <column type="field" hidden="0" name="leaf_type" width="-1"/>
      <column type="field" hidden="0" name="leaf_cycle" width="-1"/>
      <column type="actions" hidden="1" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="basin"/>
    <field editable="1" name="description"/>
    <field editable="1" name="description:en"/>
    <field editable="1" name="disused:amenity"/>
    <field editable="1" name="fee"/>
    <field editable="1" name="full_id"/>
    <field editable="1" name="landuse"/>
    <field editable="1" name="leaf_cycle"/>
    <field editable="1" name="leaf_type"/>
    <field editable="1" name="leisure"/>
    <field editable="1" name="meadow"/>
    <field editable="1" name="name"/>
    <field editable="1" name="natural"/>
    <field editable="1" name="old_name:1946-1990"/>
    <field editable="1" name="old_name:1991-1999"/>
    <field editable="1" name="osm_id"/>
    <field editable="1" name="osm_type"/>
    <field editable="1" name="ref"/>
    <field editable="1" name="sport"/>
    <field editable="1" name="type"/>
    <field editable="1" name="water"/>
    <field editable="1" name="wetland"/>
    <field editable="1" name="wikidata"/>
    <field editable="1" name="wikipedia"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="basin"/>
    <field labelOnTop="0" name="description"/>
    <field labelOnTop="0" name="description:en"/>
    <field labelOnTop="0" name="disused:amenity"/>
    <field labelOnTop="0" name="fee"/>
    <field labelOnTop="0" name="full_id"/>
    <field labelOnTop="0" name="landuse"/>
    <field labelOnTop="0" name="leaf_cycle"/>
    <field labelOnTop="0" name="leaf_type"/>
    <field labelOnTop="0" name="leisure"/>
    <field labelOnTop="0" name="meadow"/>
    <field labelOnTop="0" name="name"/>
    <field labelOnTop="0" name="natural"/>
    <field labelOnTop="0" name="old_name:1946-1990"/>
    <field labelOnTop="0" name="old_name:1991-1999"/>
    <field labelOnTop="0" name="osm_id"/>
    <field labelOnTop="0" name="osm_type"/>
    <field labelOnTop="0" name="ref"/>
    <field labelOnTop="0" name="sport"/>
    <field labelOnTop="0" name="type"/>
    <field labelOnTop="0" name="water"/>
    <field labelOnTop="0" name="wetland"/>
    <field labelOnTop="0" name="wikidata"/>
    <field labelOnTop="0" name="wikipedia"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="basin"/>
    <field reuseLastValue="0" name="description"/>
    <field reuseLastValue="0" name="description:en"/>
    <field reuseLastValue="0" name="disused:amenity"/>
    <field reuseLastValue="0" name="fee"/>
    <field reuseLastValue="0" name="full_id"/>
    <field reuseLastValue="0" name="landuse"/>
    <field reuseLastValue="0" name="leaf_cycle"/>
    <field reuseLastValue="0" name="leaf_type"/>
    <field reuseLastValue="0" name="leisure"/>
    <field reuseLastValue="0" name="meadow"/>
    <field reuseLastValue="0" name="name"/>
    <field reuseLastValue="0" name="natural"/>
    <field reuseLastValue="0" name="old_name:1946-1990"/>
    <field reuseLastValue="0" name="old_name:1991-1999"/>
    <field reuseLastValue="0" name="osm_id"/>
    <field reuseLastValue="0" name="osm_type"/>
    <field reuseLastValue="0" name="ref"/>
    <field reuseLastValue="0" name="sport"/>
    <field reuseLastValue="0" name="type"/>
    <field reuseLastValue="0" name="water"/>
    <field reuseLastValue="0" name="wetland"/>
    <field reuseLastValue="0" name="wikidata"/>
    <field reuseLastValue="0" name="wikipedia"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"old_name:1991-1999"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
