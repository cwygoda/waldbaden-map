<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis maxScale="0" minScale="100000000" simplifyLocal="1" symbologyReferenceScale="-1" readOnly="0" simplifyDrawingHints="1" version="3.22.1-Białowieża" simplifyMaxScale="1" labelsEnabled="1" simplifyAlgorithm="0" simplifyDrawingTol="1" hasScaleBasedVisibilityFlag="0" styleCategories="AllStyleCategories">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal enabled="0" fixedDuration="0" durationUnit="min" startExpression="" mode="0" endExpression="" durationField="" limitMode="0" startField="" accumulate="0" endField="">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <renderer-v2 enableorderby="0" type="RuleRenderer" referencescale="-1" forceraster="0" symbollevels="0">
    <rules key="{6d367ad1-8a39-4d02-822b-14c444ff7dcb}">
      <rule checkstate="0" key="{3c6f1c08-1b80-4efd-a6ff-96f8a9fc8c01}" symbol="0" filter="ELSE"/>
      <rule key="{5ec87356-403d-4347-acd6-cf7a2fdf7b44}" symbol="1" filter="&quot;highway&quot; in ('motorway')" label="Motorway"/>
      <rule key="{ec1b3b01-1938-4dbb-83d1-7fdab058d2a6}" symbol="2" filter="&quot;highway&quot; in ('secondary')" label="Secondary"/>
      <rule key="{30b42ef6-31d3-4feb-9f61-92dd9587d488}" symbol="3" filter="&quot;highway&quot; in ('primary')" label="Primary"/>
      <rule key="{4931b8c3-a7b2-45a0-aead-baec7833b636}" symbol="4" filter="&quot;highway&quot; in ('track', 'path', 'unclassified', 'service', 'footway', 'steps')" label="Track"/>
      <rule key="{05a5f58e-be44-424f-a353-4d51e5079a03}" symbol="5" filter="&quot;highway&quot; in ('residential', 'tertiary')" scalemindenom="1" scalemaxdenom="30000" label="Residential"/>
    </rules>
    <symbols>
      <symbol name="0" type="line" clip_to_extent="1" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="0" locked="0" class="SimpleLine">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="round"/>
            <Option name="customdash" type="QString" value="0.66;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="round"/>
            <Option name="line_color" type="QString" value="219,30,42,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="0.26"/>
            <Option name="line_width_unit" type="QString" value="MM"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="trim_distance_end" type="QString" value="0"/>
            <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_end_unit" type="QString" value="MM"/>
            <Option name="trim_distance_start" type="QString" value="0"/>
            <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_start_unit" type="QString" value="MM"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="1"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="round"/>
          <prop k="customdash" v="0.66;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="round"/>
          <prop k="line_color" v="219,30,42,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.26"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="1"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="1" type="line" clip_to_extent="1" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="10" locked="0" class="SimpleLine">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="square"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="RenderMetersInMapUnits"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="round"/>
            <Option name="line_color" type="QString" value="0,0,0,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="16.5"/>
            <Option name="line_width_unit" type="QString" value="RenderMetersInMapUnits"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="RenderMetersInMapUnits"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="trim_distance_end" type="QString" value="0"/>
            <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_end_unit" type="QString" value="MM"/>
            <Option name="trim_distance_start" type="QString" value="0"/>
            <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_start_unit" type="QString" value="MM"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="RenderMetersInMapUnits"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="round"/>
          <prop k="line_color" v="0,0,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="16.5"/>
          <prop k="line_width_unit" v="RenderMetersInMapUnits"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="RenderMetersInMapUnits"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer enabled="1" pass="11" locked="0" class="SimpleLine">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="square"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="RenderMetersInMapUnits"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="line_color" type="QString" value="255,255,255,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="13"/>
            <Option name="line_width_unit" type="QString" value="RenderMetersInMapUnits"/>
            <Option name="offset" type="QString" value="-5.55112e-17"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="RenderMetersInMapUnits"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="trim_distance_end" type="QString" value="0"/>
            <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_end_unit" type="QString" value="MM"/>
            <Option name="trim_distance_start" type="QString" value="0"/>
            <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_start_unit" type="QString" value="MM"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="RenderMetersInMapUnits"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="255,255,255,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="13"/>
          <prop k="line_width_unit" v="RenderMetersInMapUnits"/>
          <prop k="offset" v="-5.55112e-17"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="RenderMetersInMapUnits"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="2" type="line" clip_to_extent="1" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="0" locked="1" class="SimpleLine">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="round"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="round"/>
            <Option name="line_color" type="QString" value="0,0,0,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="3.46"/>
            <Option name="line_width_unit" type="QString" value="MM"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="trim_distance_end" type="QString" value="0"/>
            <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_end_unit" type="QString" value="MM"/>
            <Option name="trim_distance_start" type="QString" value="0"/>
            <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_start_unit" type="QString" value="MM"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="round"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="round"/>
          <prop k="line_color" v="0,0,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="3.46"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer enabled="1" pass="2" locked="0" class="SimpleLine">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="round"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="round"/>
            <Option name="line_color" type="QString" value="252,214,0,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="2.80717"/>
            <Option name="line_width_unit" type="QString" value="MM"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="trim_distance_end" type="QString" value="0"/>
            <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_end_unit" type="QString" value="MM"/>
            <Option name="trim_distance_start" type="QString" value="0"/>
            <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_start_unit" type="QString" value="MM"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="round"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="round"/>
          <prop k="line_color" v="252,214,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="2.80717"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="3" type="line" clip_to_extent="1" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="0" locked="1" class="SimpleLine">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="round"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="round"/>
            <Option name="line_color" type="QString" value="0,0,0,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="1.06"/>
            <Option name="line_width_unit" type="QString" value="MM"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="trim_distance_end" type="QString" value="0"/>
            <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_end_unit" type="QString" value="MM"/>
            <Option name="trim_distance_start" type="QString" value="0"/>
            <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_start_unit" type="QString" value="MM"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="round"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="round"/>
          <prop k="line_color" v="0,0,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="1.06"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer enabled="1" pass="2" locked="0" class="SimpleLine">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="round"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="round"/>
            <Option name="line_color" type="QString" value="252,214,0,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="0.86"/>
            <Option name="line_width_unit" type="QString" value="MM"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="trim_distance_end" type="QString" value="0"/>
            <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_end_unit" type="QString" value="MM"/>
            <Option name="trim_distance_start" type="QString" value="0"/>
            <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_start_unit" type="QString" value="MM"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="round"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="round"/>
          <prop k="line_color" v="252,214,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.86"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="4" type="line" clip_to_extent="1" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="0" locked="0" class="SimpleLine">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="square"/>
            <Option name="customdash" type="QString" value="3;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="bevel"/>
            <Option name="line_color" type="QString" value="0,0,0,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="0.36"/>
            <Option name="line_width_unit" type="QString" value="MM"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="trim_distance_end" type="QString" value="0"/>
            <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_end_unit" type="QString" value="MM"/>
            <Option name="trim_distance_start" type="QString" value="0"/>
            <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_start_unit" type="QString" value="MM"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="1"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="square"/>
          <prop k="customdash" v="3;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="line_color" v="0,0,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="0.36"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="1"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol name="5" type="line" clip_to_extent="1" force_rhr="0" alpha="1">
        <data_defined_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </data_defined_properties>
        <layer enabled="1" pass="0" locked="1" class="SimpleLine">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="round"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="round"/>
            <Option name="line_color" type="QString" value="0,0,0,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="3"/>
            <Option name="line_width_unit" type="QString" value="MM"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="trim_distance_end" type="QString" value="0"/>
            <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_end_unit" type="QString" value="MM"/>
            <Option name="trim_distance_start" type="QString" value="0"/>
            <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_start_unit" type="QString" value="MM"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="round"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="round"/>
          <prop k="line_color" v="0,0,0,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="3"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer enabled="1" pass="1" locked="0" class="SimpleLine">
          <Option type="Map">
            <Option name="align_dash_pattern" type="QString" value="0"/>
            <Option name="capstyle" type="QString" value="round"/>
            <Option name="customdash" type="QString" value="5;2"/>
            <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="customdash_unit" type="QString" value="MM"/>
            <Option name="dash_pattern_offset" type="QString" value="0"/>
            <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
            <Option name="draw_inside_polygon" type="QString" value="0"/>
            <Option name="joinstyle" type="QString" value="round"/>
            <Option name="line_color" type="QString" value="255,255,255,255"/>
            <Option name="line_style" type="QString" value="solid"/>
            <Option name="line_width" type="QString" value="2.43396"/>
            <Option name="line_width_unit" type="QString" value="MM"/>
            <Option name="offset" type="QString" value="0"/>
            <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="offset_unit" type="QString" value="MM"/>
            <Option name="ring_filter" type="QString" value="0"/>
            <Option name="trim_distance_end" type="QString" value="0"/>
            <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_end_unit" type="QString" value="MM"/>
            <Option name="trim_distance_start" type="QString" value="0"/>
            <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            <Option name="trim_distance_start_unit" type="QString" value="MM"/>
            <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
            <Option name="use_custom_dash" type="QString" value="0"/>
            <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
          </Option>
          <prop k="align_dash_pattern" v="0"/>
          <prop k="capstyle" v="round"/>
          <prop k="customdash" v="5;2"/>
          <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="customdash_unit" v="MM"/>
          <prop k="dash_pattern_offset" v="0"/>
          <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="dash_pattern_offset_unit" v="MM"/>
          <prop k="draw_inside_polygon" v="0"/>
          <prop k="joinstyle" v="round"/>
          <prop k="line_color" v="255,255,255,255"/>
          <prop k="line_style" v="solid"/>
          <prop k="line_width" v="2.43396"/>
          <prop k="line_width_unit" v="MM"/>
          <prop k="offset" v="0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="ring_filter" v="0"/>
          <prop k="trim_distance_end" v="0"/>
          <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_end_unit" v="MM"/>
          <prop k="trim_distance_start" v="0"/>
          <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="trim_distance_start_unit" v="MM"/>
          <prop k="tweak_dash_pattern_on_corners" v="0"/>
          <prop k="use_custom_dash" v="0"/>
          <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style multilineHeight="1" previewBkgrdColor="255,255,255,255" isExpression="0" legendString="Aa" fontStrikeout="0" fontKerning="1" namedStyle="Regular" fontLetterSpacing="0" fontWordSpacing="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0" textOpacity="1" textColor="50,50,50,255" fontUnderline="0" fontWeight="50" fontSize="10" fontItalic="0" useSubstitutions="0" textOrientation="horizontal" capitalization="0" fieldName="name" fontSizeUnit="Point" blendMode="0" allowHtml="0" fontFamily="Liberation Sans">
        <families/>
        <text-buffer bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferColor="255,255,255,255" bufferBlendMode="0" bufferOpacity="1" bufferNoFill="0" bufferJoinStyle="128" bufferSize="0" bufferDraw="0" bufferSizeUnits="MM"/>
        <text-mask maskEnabled="0" maskType="0" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskJoinStyle="128" maskSize="0" maskSizeUnits="MM" maskOpacity="1" maskedSymbolLayers=""/>
        <background shapeSVGFile="" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeBorderWidth="0" shapeOpacity="1" shapeOffsetX="0" shapeSizeUnit="Point" shapeRadiiUnit="Point" shapeRotationType="0" shapeSizeY="0" shapeOffsetUnit="Point" shapeType="0" shapeFillColor="255,255,255,255" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeSizeX="0" shapeJoinStyle="64" shapeDraw="0" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeRadiiY="0" shapeRotation="0" shapeOffsetY="0" shapeSizeType="0" shapeBorderColor="128,128,128,255" shapeBlendMode="0" shapeBorderWidthUnit="Point" shapeRadiiX="0">
          <symbol name="markerSymbol" type="marker" clip_to_extent="1" force_rhr="0" alpha="1">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
            <layer enabled="1" pass="0" locked="0" class="SimpleMarker">
              <Option type="Map">
                <Option name="angle" type="QString" value="0"/>
                <Option name="cap_style" type="QString" value="square"/>
                <Option name="color" type="QString" value="133,182,111,255"/>
                <Option name="horizontal_anchor_point" type="QString" value="1"/>
                <Option name="joinstyle" type="QString" value="bevel"/>
                <Option name="name" type="QString" value="circle"/>
                <Option name="offset" type="QString" value="0,0"/>
                <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                <Option name="offset_unit" type="QString" value="MM"/>
                <Option name="outline_color" type="QString" value="35,35,35,255"/>
                <Option name="outline_style" type="QString" value="solid"/>
                <Option name="outline_width" type="QString" value="0"/>
                <Option name="outline_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                <Option name="outline_width_unit" type="QString" value="MM"/>
                <Option name="scale_method" type="QString" value="diameter"/>
                <Option name="size" type="QString" value="2"/>
                <Option name="size_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                <Option name="size_unit" type="QString" value="MM"/>
                <Option name="vertical_anchor_point" type="QString" value="1"/>
              </Option>
              <prop k="angle" v="0"/>
              <prop k="cap_style" v="square"/>
              <prop k="color" v="133,182,111,255"/>
              <prop k="horizontal_anchor_point" v="1"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="name" v="circle"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="scale_method" v="diameter"/>
              <prop k="size" v="2"/>
              <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="size_unit" v="MM"/>
              <prop k="vertical_anchor_point" v="1"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" type="QString" value=""/>
                  <Option name="properties"/>
                  <Option name="type" type="QString" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
          <symbol name="fillSymbol" type="fill" clip_to_extent="1" force_rhr="0" alpha="1">
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
            <layer enabled="1" pass="0" locked="0" class="SimpleFill">
              <Option type="Map">
                <Option name="border_width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                <Option name="color" type="QString" value="255,255,255,255"/>
                <Option name="joinstyle" type="QString" value="bevel"/>
                <Option name="offset" type="QString" value="0,0"/>
                <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
                <Option name="offset_unit" type="QString" value="MM"/>
                <Option name="outline_color" type="QString" value="128,128,128,255"/>
                <Option name="outline_style" type="QString" value="no"/>
                <Option name="outline_width" type="QString" value="0"/>
                <Option name="outline_width_unit" type="QString" value="Point"/>
                <Option name="style" type="QString" value="solid"/>
              </Option>
              <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="color" v="255,255,255,255"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="128,128,128,255"/>
              <prop k="outline_style" v="no"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_unit" v="Point"/>
              <prop k="style" v="solid"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" type="QString" value=""/>
                  <Option name="properties"/>
                  <Option name="type" type="QString" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowBlendMode="6" shadowOffsetGlobal="1" shadowDraw="0" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOpacity="1" shadowColor="0,0,0,255" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowRadiusAlphaOnly="0" shadowRadius="1.5" shadowUnder="0" shadowOffsetAngle="135" shadowRadiusUnit="Point" shadowScale="100" shadowOffsetDist="1" shadowOffsetUnit="Point"/>
        <dd_properties>
          <Option type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format addDirectionSymbol="0" multilineAlign="0" rightDirectionSymbol=">" leftDirectionSymbol="&lt;" reverseDirectionSymbol="0" formatNumbers="0" placeDirectionSymbol="0" useMaxLineLengthForAutoWrap="1" decimals="3" wrapChar="" autoWrapLength="0" plussign="0"/>
      <placement rotationUnit="AngleDegrees" yOffset="0" polygonPlacementFlags="2" preserveRotation="1" geometryGeneratorEnabled="0" maxCurvedCharAngleIn="25" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" geometryGeneratorType="PointGeometry" geometryGenerator="" centroidInside="0" xOffset="0" placementFlags="9" placement="3" repeatDistance="0" lineAnchorType="0" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" offsetType="0" lineAnchorPercent="0.5" overrunDistanceUnit="MM" centroidWhole="0" distUnits="MM" maxCurvedCharAngleOut="-25" distMapUnitScale="3x:0,0,0,0,0,0" fitInPolygonOnly="0" quadOffset="4" overrunDistance="0" dist="0" layerType="LineGeometry" rotationAngle="0" offsetUnits="MM" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" priority="5" repeatDistanceUnits="MM" lineAnchorClipping="0"/>
      <rendering fontMinPixelSize="3" scaleVisibility="0" obstacle="1" obstacleFactor="1" minFeatureSize="0" mergeLines="0" scaleMin="0" labelPerPart="0" displayAll="0" fontMaxPixelSize="10000" drawLabels="1" limitNumLabels="0" scaleMax="0" zIndex="0" fontLimitPixelSize="0" upsidedownLabels="0" maxNumLabels="2000" obstacleType="1" unplacedVisibility="0"/>
      <dd_properties>
        <Option type="Map">
          <Option name="name" type="QString" value=""/>
          <Option name="properties"/>
          <Option name="type" type="QString" value="collection"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option name="anchorPoint" type="QString" value="pole_of_inaccessibility"/>
          <Option name="blendMode" type="int" value="0"/>
          <Option name="ddProperties" type="Map">
            <Option name="name" type="QString" value=""/>
            <Option name="properties"/>
            <Option name="type" type="QString" value="collection"/>
          </Option>
          <Option name="drawToAllParts" type="bool" value="false"/>
          <Option name="enabled" type="QString" value="0"/>
          <Option name="labelAnchorPoint" type="QString" value="point_on_exterior"/>
          <Option name="lineSymbol" type="QString" value="&lt;symbol name=&quot;symbol&quot; type=&quot;line&quot; clip_to_extent=&quot;1&quot; force_rhr=&quot;0&quot; alpha=&quot;1&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option name=&quot;name&quot; type=&quot;QString&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option name=&quot;type&quot; type=&quot;QString&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer enabled=&quot;1&quot; pass=&quot;0&quot; locked=&quot;0&quot; class=&quot;SimpleLine&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option name=&quot;align_dash_pattern&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;capstyle&quot; type=&quot;QString&quot; value=&quot;square&quot;/>&lt;Option name=&quot;customdash&quot; type=&quot;QString&quot; value=&quot;5;2&quot;/>&lt;Option name=&quot;customdash_map_unit_scale&quot; type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option name=&quot;customdash_unit&quot; type=&quot;QString&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;dash_pattern_offset&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;dash_pattern_offset_map_unit_scale&quot; type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option name=&quot;dash_pattern_offset_unit&quot; type=&quot;QString&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;draw_inside_polygon&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;joinstyle&quot; type=&quot;QString&quot; value=&quot;bevel&quot;/>&lt;Option name=&quot;line_color&quot; type=&quot;QString&quot; value=&quot;60,60,60,255&quot;/>&lt;Option name=&quot;line_style&quot; type=&quot;QString&quot; value=&quot;solid&quot;/>&lt;Option name=&quot;line_width&quot; type=&quot;QString&quot; value=&quot;0.3&quot;/>&lt;Option name=&quot;line_width_unit&quot; type=&quot;QString&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;offset&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;offset_map_unit_scale&quot; type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option name=&quot;offset_unit&quot; type=&quot;QString&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;ring_filter&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;trim_distance_end&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;trim_distance_end_map_unit_scale&quot; type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option name=&quot;trim_distance_end_unit&quot; type=&quot;QString&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;trim_distance_start&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;trim_distance_start_map_unit_scale&quot; type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;Option name=&quot;trim_distance_start_unit&quot; type=&quot;QString&quot; value=&quot;MM&quot;/>&lt;Option name=&quot;tweak_dash_pattern_on_corners&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;use_custom_dash&quot; type=&quot;QString&quot; value=&quot;0&quot;/>&lt;Option name=&quot;width_map_unit_scale&quot; type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot;/>&lt;/Option>&lt;prop k=&quot;align_dash_pattern&quot; v=&quot;0&quot;/>&lt;prop k=&quot;capstyle&quot; v=&quot;square&quot;/>&lt;prop k=&quot;customdash&quot; v=&quot;5;2&quot;/>&lt;prop k=&quot;customdash_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;customdash_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;dash_pattern_offset&quot; v=&quot;0&quot;/>&lt;prop k=&quot;dash_pattern_offset_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;dash_pattern_offset_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;draw_inside_polygon&quot; v=&quot;0&quot;/>&lt;prop k=&quot;joinstyle&quot; v=&quot;bevel&quot;/>&lt;prop k=&quot;line_color&quot; v=&quot;60,60,60,255&quot;/>&lt;prop k=&quot;line_style&quot; v=&quot;solid&quot;/>&lt;prop k=&quot;line_width&quot; v=&quot;0.3&quot;/>&lt;prop k=&quot;line_width_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;offset&quot; v=&quot;0&quot;/>&lt;prop k=&quot;offset_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;offset_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;ring_filter&quot; v=&quot;0&quot;/>&lt;prop k=&quot;trim_distance_end&quot; v=&quot;0&quot;/>&lt;prop k=&quot;trim_distance_end_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;trim_distance_end_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;trim_distance_start&quot; v=&quot;0&quot;/>&lt;prop k=&quot;trim_distance_start_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;trim_distance_start_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;tweak_dash_pattern_on_corners&quot; v=&quot;0&quot;/>&lt;prop k=&quot;use_custom_dash&quot; v=&quot;0&quot;/>&lt;prop k=&quot;width_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option name=&quot;name&quot; type=&quot;QString&quot; value=&quot;&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option name=&quot;type&quot; type=&quot;QString&quot; value=&quot;collection&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>"/>
          <Option name="minLength" type="double" value="0"/>
          <Option name="minLengthMapUnitScale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="minLengthUnit" type="QString" value="MM"/>
          <Option name="offsetFromAnchor" type="double" value="0"/>
          <Option name="offsetFromAnchorMapUnitScale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="offsetFromAnchorUnit" type="QString" value="MM"/>
          <Option name="offsetFromLabel" type="double" value="0"/>
          <Option name="offsetFromLabelMapUnitScale" type="QString" value="3x:0,0,0,0,0,0"/>
          <Option name="offsetFromLabelUnit" type="QString" value="MM"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option name="embeddedWidgets/count" type="int" value="0"/>
      <Option name="variableNames" type="StringList">
        <Option type="QString" value="quickosm_query"/>
      </Option>
      <Option name="variableValues" type="StringList">
        <Option type="QString" value="[out:xml] [timeout:25];&#xa;(&#xa;    node[&quot;highway&quot;]( 52.74119,13.4638,52.78308,13.59963);&#xa;    way[&quot;highway&quot;]( 52.74119,13.4638,52.78308,13.59963);&#xa;    relation[&quot;highway&quot;]( 52.74119,13.4638,52.78308,13.59963);&#xa;);&#xa;(._;>;);&#xa;out body;"/>
      </Option>
    </Option>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer diagramType="Histogram" attributeLegend="1">
    <DiagramCategory backgroundAlpha="255" sizeType="MM" sizeScale="3x:0,0,0,0,0,0" scaleDependency="Area" minScaleDenominator="0" spacingUnit="MM" penAlpha="255" penColor="#000000" showAxis="1" direction="0" scaleBasedVisibility="0" backgroundColor="#ffffff" penWidth="0" labelPlacementMethod="XHeight" spacing="5" rotationOffset="270" spacingUnitScale="3x:0,0,0,0,0,0" width="15" lineSizeType="MM" enabled="0" height="15" diagramOrientation="Up" minimumSize="0" maxScaleDenominator="1e+08" barWidth="5" lineSizeScale="3x:0,0,0,0,0,0" opacity="1">
      <fontProperties style="" description="Cantarell,11,-1,5,50,0,0,0,0,0"/>
      <attribute color="#000000" field="" label=""/>
      <axisSymbol>
        <symbol name="" type="line" clip_to_extent="1" force_rhr="0" alpha="1">
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
          <layer enabled="1" pass="0" locked="0" class="SimpleLine">
            <Option type="Map">
              <Option name="align_dash_pattern" type="QString" value="0"/>
              <Option name="capstyle" type="QString" value="square"/>
              <Option name="customdash" type="QString" value="5;2"/>
              <Option name="customdash_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="customdash_unit" type="QString" value="MM"/>
              <Option name="dash_pattern_offset" type="QString" value="0"/>
              <Option name="dash_pattern_offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="dash_pattern_offset_unit" type="QString" value="MM"/>
              <Option name="draw_inside_polygon" type="QString" value="0"/>
              <Option name="joinstyle" type="QString" value="bevel"/>
              <Option name="line_color" type="QString" value="35,35,35,255"/>
              <Option name="line_style" type="QString" value="solid"/>
              <Option name="line_width" type="QString" value="0.26"/>
              <Option name="line_width_unit" type="QString" value="MM"/>
              <Option name="offset" type="QString" value="0"/>
              <Option name="offset_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="offset_unit" type="QString" value="MM"/>
              <Option name="ring_filter" type="QString" value="0"/>
              <Option name="trim_distance_end" type="QString" value="0"/>
              <Option name="trim_distance_end_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="trim_distance_end_unit" type="QString" value="MM"/>
              <Option name="trim_distance_start" type="QString" value="0"/>
              <Option name="trim_distance_start_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
              <Option name="trim_distance_start_unit" type="QString" value="MM"/>
              <Option name="tweak_dash_pattern_on_corners" type="QString" value="0"/>
              <Option name="use_custom_dash" type="QString" value="0"/>
              <Option name="width_map_unit_scale" type="QString" value="3x:0,0,0,0,0,0"/>
            </Option>
            <prop k="align_dash_pattern" v="0"/>
            <prop k="capstyle" v="square"/>
            <prop k="customdash" v="5;2"/>
            <prop k="customdash_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="customdash_unit" v="MM"/>
            <prop k="dash_pattern_offset" v="0"/>
            <prop k="dash_pattern_offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="dash_pattern_offset_unit" v="MM"/>
            <prop k="draw_inside_polygon" v="0"/>
            <prop k="joinstyle" v="bevel"/>
            <prop k="line_color" v="35,35,35,255"/>
            <prop k="line_style" v="solid"/>
            <prop k="line_width" v="0.26"/>
            <prop k="line_width_unit" v="MM"/>
            <prop k="offset" v="0"/>
            <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="offset_unit" v="MM"/>
            <prop k="ring_filter" v="0"/>
            <prop k="trim_distance_end" v="0"/>
            <prop k="trim_distance_end_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_end_unit" v="MM"/>
            <prop k="trim_distance_start" v="0"/>
            <prop k="trim_distance_start_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <prop k="trim_distance_start_unit" v="MM"/>
            <prop k="tweak_dash_pattern_on_corners" v="0"/>
            <prop k="use_custom_dash" v="0"/>
            <prop k="width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
            <data_defined_properties>
              <Option type="Map">
                <Option name="name" type="QString" value=""/>
                <Option name="properties"/>
                <Option name="type" type="QString" value="collection"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" zIndex="0" priority="0" placement="2" dist="0" linePlacementFlags="18" showAll="1">
    <properties>
      <Option type="Map">
        <Option name="name" type="QString" value=""/>
        <Option name="properties"/>
        <Option name="type" type="QString" value="collection"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="None" name="full_id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="osm_id">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="osm_type">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="highway">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="maxspeed:practical">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="proposed">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="flood_prone">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ford">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="destination:ref:lanes">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="destination:colour:backward">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="footway:surface">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cycleway:surface">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="is_sidepath:of:name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="is_sidepath:of">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="grade">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="toilets:wheelchair">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="shelter">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="addr:street">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="addr:city">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="delivery">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="man_made">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="usage">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="tracks">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="railway">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="gauge">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="electrified">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="disused:railway">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="mtb:scale:uphill">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="note_2">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="source:maxspeed:forward">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="source:maxspeed:backward">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="levelpart">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="building:part">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="mtb:scale:imba">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="alt_name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="node">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="destination:ref:forward">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="is_sidepath">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="abandoned:highway">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="length">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="public_transport">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="maxwidth">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="crossing:island">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="crossing">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="destination:lanes">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="sulky">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cycleway:right:segregated">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cycleway:left:lane">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="fixme_1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="sac_scale">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="addr:postcode">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="maxspeed:hgv">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="maxspeed:caravan">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="turn:lanes:forward">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="check_date:tracktype">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="maxweight:signed">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="change:lanes">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="amenity">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="footway">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="opening_date">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="construction">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="maxweight:forestry">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="maxweight:agricultural">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="sidewalk:right:bicycle">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="loc_name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="direction">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="material">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="handrail:right">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="surface:middle">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="wikipedia">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="wikidata">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="tactile_paving">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="handrail">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="psv">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="vehicle">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="bicycle:backward">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="parking:lane:left">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="parking:condition:left">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="alley">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="name:etymology:wikidata">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="overtaking">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="destination:ref:backward">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="image">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="heritage:operator">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="heritage">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="bldam:criteria">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="parking:lane:both">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ramp:bicycle">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ramp">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="snowmobile">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ski">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="noexit">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="noname">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="surface:note">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="check_date:surface">
      <editWidget type="DateTime">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="step_count">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="start_date">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="check_date">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="destination:backward">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="sidewalk:both:bicycle">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="destination:symbol">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="split_from">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="bus">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="tunnel">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="indoor">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="snowplowing">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="motorcycle">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="motorcar">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="goods">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="destination:lanes:backward">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="hgv:maxweight">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="destination:symbol:forward">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="informal">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="level">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="turn:lanes:backward">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="lanes:forward">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="lanes:backward">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cycleway:both:segregated">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="note:de">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="maxweight:conditional">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="zone:maxspeed">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="source:width">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="parking:lane:right">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="parking:condition:right:time_interval">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="parking:condition:right">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cycleway:right:surface">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cycleway:right:smoothness">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cycleway:left:surface">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cycleway:left:smoothness">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cycleway:both:lane">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="traffic_calming">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="traffic_sign">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="stroller">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="service">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="mtb:scale">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="incline">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="covered">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="bicycle_road">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="old_name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="segregated">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="horse">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="surface:lanes">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="unsigned_ref">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="access">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="maxweight">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="zone:traffic">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="old_ref">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="wheelchair">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="trail_visibility">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="oneway:bicycle">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="motor_vehicle">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="width">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="destination:colour">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="destination">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="toll:N3">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="motorroad">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="embankment">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cycleway">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="lane_markings">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="description">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="sidewalk:left">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="sidewalk:both">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="priority_road">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cycleway:left">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="turn:lanes">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="placement">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="maxspeed:type">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cycleway:both">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="source:lit">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="maxspeed:forward">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="maxspeed:backward">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="maxspeed:conditional">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="tracktype">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="disused:aeroway">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="maxheight">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="reg_name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="note:name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="layer">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="int_ref">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="bridge:name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="bridge">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="source:maxspeed">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="sidewalk">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="postal_code">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="lit">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="sidewalk:right">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="junction">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="foot">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="cycleway:right">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="bicycle">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="oneway">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="destination:ref">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="hgv">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="surface">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="smoothness">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="ref">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="name">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="maxspeed">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="lanes">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="destination:ref:to:forward">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="destination:forward">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="None" name="destination:colour:forward">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias name="" index="0" field="full_id"/>
    <alias name="" index="1" field="osm_id"/>
    <alias name="" index="2" field="osm_type"/>
    <alias name="" index="3" field="highway"/>
    <alias name="" index="4" field="maxspeed:practical"/>
    <alias name="" index="5" field="proposed"/>
    <alias name="" index="6" field="flood_prone"/>
    <alias name="" index="7" field="ford"/>
    <alias name="" index="8" field="destination:ref:lanes"/>
    <alias name="" index="9" field="destination:colour:backward"/>
    <alias name="" index="10" field="footway:surface"/>
    <alias name="" index="11" field="cycleway:surface"/>
    <alias name="" index="12" field="is_sidepath:of:name"/>
    <alias name="" index="13" field="is_sidepath:of"/>
    <alias name="" index="14" field="grade"/>
    <alias name="" index="15" field="toilets:wheelchair"/>
    <alias name="" index="16" field="shelter"/>
    <alias name="" index="17" field="addr:street"/>
    <alias name="" index="18" field="addr:city"/>
    <alias name="" index="19" field="delivery"/>
    <alias name="" index="20" field="man_made"/>
    <alias name="" index="21" field="usage"/>
    <alias name="" index="22" field="tracks"/>
    <alias name="" index="23" field="railway"/>
    <alias name="" index="24" field="gauge"/>
    <alias name="" index="25" field="electrified"/>
    <alias name="" index="26" field="disused:railway"/>
    <alias name="" index="27" field="mtb:scale:uphill"/>
    <alias name="" index="28" field="note_2"/>
    <alias name="" index="29" field="source:maxspeed:forward"/>
    <alias name="" index="30" field="source:maxspeed:backward"/>
    <alias name="" index="31" field="levelpart"/>
    <alias name="" index="32" field="building:part"/>
    <alias name="" index="33" field="mtb:scale:imba"/>
    <alias name="" index="34" field="alt_name"/>
    <alias name="" index="35" field="node"/>
    <alias name="" index="36" field="destination:ref:forward"/>
    <alias name="" index="37" field="is_sidepath"/>
    <alias name="" index="38" field="abandoned:highway"/>
    <alias name="" index="39" field="length"/>
    <alias name="" index="40" field="public_transport"/>
    <alias name="" index="41" field="maxwidth"/>
    <alias name="" index="42" field="crossing:island"/>
    <alias name="" index="43" field="crossing"/>
    <alias name="" index="44" field="destination:lanes"/>
    <alias name="" index="45" field="sulky"/>
    <alias name="" index="46" field="cycleway:right:segregated"/>
    <alias name="" index="47" field="cycleway:left:lane"/>
    <alias name="" index="48" field="fixme_1"/>
    <alias name="" index="49" field="sac_scale"/>
    <alias name="" index="50" field="addr:postcode"/>
    <alias name="" index="51" field="maxspeed:hgv"/>
    <alias name="" index="52" field="maxspeed:caravan"/>
    <alias name="" index="53" field="turn:lanes:forward"/>
    <alias name="" index="54" field="check_date:tracktype"/>
    <alias name="" index="55" field="maxweight:signed"/>
    <alias name="" index="56" field="change:lanes"/>
    <alias name="" index="57" field="amenity"/>
    <alias name="" index="58" field="footway"/>
    <alias name="" index="59" field="opening_date"/>
    <alias name="" index="60" field="construction"/>
    <alias name="" index="61" field="maxweight:forestry"/>
    <alias name="" index="62" field="maxweight:agricultural"/>
    <alias name="" index="63" field="sidewalk:right:bicycle"/>
    <alias name="" index="64" field="loc_name"/>
    <alias name="" index="65" field="direction"/>
    <alias name="" index="66" field="material"/>
    <alias name="" index="67" field="handrail:right"/>
    <alias name="" index="68" field="surface:middle"/>
    <alias name="" index="69" field="wikipedia"/>
    <alias name="" index="70" field="wikidata"/>
    <alias name="" index="71" field="tactile_paving"/>
    <alias name="" index="72" field="handrail"/>
    <alias name="" index="73" field="psv"/>
    <alias name="" index="74" field="vehicle"/>
    <alias name="" index="75" field="bicycle:backward"/>
    <alias name="" index="76" field="parking:lane:left"/>
    <alias name="" index="77" field="parking:condition:left"/>
    <alias name="" index="78" field="alley"/>
    <alias name="" index="79" field="name:etymology:wikidata"/>
    <alias name="" index="80" field="overtaking"/>
    <alias name="" index="81" field="destination:ref:backward"/>
    <alias name="" index="82" field="image"/>
    <alias name="" index="83" field="heritage:operator"/>
    <alias name="" index="84" field="heritage"/>
    <alias name="" index="85" field="bldam:criteria"/>
    <alias name="" index="86" field="parking:lane:both"/>
    <alias name="" index="87" field="ramp:bicycle"/>
    <alias name="" index="88" field="ramp"/>
    <alias name="" index="89" field="snowmobile"/>
    <alias name="" index="90" field="ski"/>
    <alias name="" index="91" field="noexit"/>
    <alias name="" index="92" field="noname"/>
    <alias name="" index="93" field="surface:note"/>
    <alias name="" index="94" field="check_date:surface"/>
    <alias name="" index="95" field="step_count"/>
    <alias name="" index="96" field="start_date"/>
    <alias name="" index="97" field="check_date"/>
    <alias name="" index="98" field="destination:backward"/>
    <alias name="" index="99" field="sidewalk:both:bicycle"/>
    <alias name="" index="100" field="destination:symbol"/>
    <alias name="" index="101" field="split_from"/>
    <alias name="" index="102" field="bus"/>
    <alias name="" index="103" field="tunnel"/>
    <alias name="" index="104" field="indoor"/>
    <alias name="" index="105" field="snowplowing"/>
    <alias name="" index="106" field="motorcycle"/>
    <alias name="" index="107" field="motorcar"/>
    <alias name="" index="108" field="goods"/>
    <alias name="" index="109" field="destination:lanes:backward"/>
    <alias name="" index="110" field="hgv:maxweight"/>
    <alias name="" index="111" field="destination:symbol:forward"/>
    <alias name="" index="112" field="informal"/>
    <alias name="" index="113" field="level"/>
    <alias name="" index="114" field="turn:lanes:backward"/>
    <alias name="" index="115" field="lanes:forward"/>
    <alias name="" index="116" field="lanes:backward"/>
    <alias name="" index="117" field="cycleway:both:segregated"/>
    <alias name="" index="118" field="note:de"/>
    <alias name="" index="119" field="maxweight:conditional"/>
    <alias name="" index="120" field="zone:maxspeed"/>
    <alias name="" index="121" field="source:width"/>
    <alias name="" index="122" field="parking:lane:right"/>
    <alias name="" index="123" field="parking:condition:right:time_interval"/>
    <alias name="" index="124" field="parking:condition:right"/>
    <alias name="" index="125" field="cycleway:right:surface"/>
    <alias name="" index="126" field="cycleway:right:smoothness"/>
    <alias name="" index="127" field="cycleway:left:surface"/>
    <alias name="" index="128" field="cycleway:left:smoothness"/>
    <alias name="" index="129" field="cycleway:both:lane"/>
    <alias name="" index="130" field="traffic_calming"/>
    <alias name="" index="131" field="traffic_sign"/>
    <alias name="" index="132" field="stroller"/>
    <alias name="" index="133" field="service"/>
    <alias name="" index="134" field="mtb:scale"/>
    <alias name="" index="135" field="incline"/>
    <alias name="" index="136" field="covered"/>
    <alias name="" index="137" field="bicycle_road"/>
    <alias name="" index="138" field="old_name"/>
    <alias name="" index="139" field="segregated"/>
    <alias name="" index="140" field="horse"/>
    <alias name="" index="141" field="surface:lanes"/>
    <alias name="" index="142" field="unsigned_ref"/>
    <alias name="" index="143" field="access"/>
    <alias name="" index="144" field="maxweight"/>
    <alias name="" index="145" field="zone:traffic"/>
    <alias name="" index="146" field="old_ref"/>
    <alias name="" index="147" field="wheelchair"/>
    <alias name="" index="148" field="trail_visibility"/>
    <alias name="" index="149" field="oneway:bicycle"/>
    <alias name="" index="150" field="motor_vehicle"/>
    <alias name="" index="151" field="width"/>
    <alias name="" index="152" field="destination:colour"/>
    <alias name="" index="153" field="destination"/>
    <alias name="" index="154" field="toll:N3"/>
    <alias name="" index="155" field="motorroad"/>
    <alias name="" index="156" field="embankment"/>
    <alias name="" index="157" field="cycleway"/>
    <alias name="" index="158" field="lane_markings"/>
    <alias name="" index="159" field="description"/>
    <alias name="" index="160" field="sidewalk:left"/>
    <alias name="" index="161" field="sidewalk:both"/>
    <alias name="" index="162" field="priority_road"/>
    <alias name="" index="163" field="cycleway:left"/>
    <alias name="" index="164" field="turn:lanes"/>
    <alias name="" index="165" field="placement"/>
    <alias name="" index="166" field="maxspeed:type"/>
    <alias name="" index="167" field="cycleway:both"/>
    <alias name="" index="168" field="source:lit"/>
    <alias name="" index="169" field="maxspeed:forward"/>
    <alias name="" index="170" field="maxspeed:backward"/>
    <alias name="" index="171" field="maxspeed:conditional"/>
    <alias name="" index="172" field="tracktype"/>
    <alias name="" index="173" field="disused:aeroway"/>
    <alias name="" index="174" field="maxheight"/>
    <alias name="" index="175" field="reg_name"/>
    <alias name="" index="176" field="note:name"/>
    <alias name="" index="177" field="layer"/>
    <alias name="" index="178" field="int_ref"/>
    <alias name="" index="179" field="bridge:name"/>
    <alias name="" index="180" field="bridge"/>
    <alias name="" index="181" field="source:maxspeed"/>
    <alias name="" index="182" field="sidewalk"/>
    <alias name="" index="183" field="postal_code"/>
    <alias name="" index="184" field="lit"/>
    <alias name="" index="185" field="sidewalk:right"/>
    <alias name="" index="186" field="junction"/>
    <alias name="" index="187" field="foot"/>
    <alias name="" index="188" field="cycleway:right"/>
    <alias name="" index="189" field="bicycle"/>
    <alias name="" index="190" field="oneway"/>
    <alias name="" index="191" field="destination:ref"/>
    <alias name="" index="192" field="hgv"/>
    <alias name="" index="193" field="surface"/>
    <alias name="" index="194" field="smoothness"/>
    <alias name="" index="195" field="ref"/>
    <alias name="" index="196" field="name"/>
    <alias name="" index="197" field="maxspeed"/>
    <alias name="" index="198" field="lanes"/>
    <alias name="" index="199" field="destination:ref:to:forward"/>
    <alias name="" index="200" field="destination:forward"/>
    <alias name="" index="201" field="destination:colour:forward"/>
  </aliases>
  <defaults>
    <default applyOnUpdate="0" field="full_id" expression=""/>
    <default applyOnUpdate="0" field="osm_id" expression=""/>
    <default applyOnUpdate="0" field="osm_type" expression=""/>
    <default applyOnUpdate="0" field="highway" expression=""/>
    <default applyOnUpdate="0" field="maxspeed:practical" expression=""/>
    <default applyOnUpdate="0" field="proposed" expression=""/>
    <default applyOnUpdate="0" field="flood_prone" expression=""/>
    <default applyOnUpdate="0" field="ford" expression=""/>
    <default applyOnUpdate="0" field="destination:ref:lanes" expression=""/>
    <default applyOnUpdate="0" field="destination:colour:backward" expression=""/>
    <default applyOnUpdate="0" field="footway:surface" expression=""/>
    <default applyOnUpdate="0" field="cycleway:surface" expression=""/>
    <default applyOnUpdate="0" field="is_sidepath:of:name" expression=""/>
    <default applyOnUpdate="0" field="is_sidepath:of" expression=""/>
    <default applyOnUpdate="0" field="grade" expression=""/>
    <default applyOnUpdate="0" field="toilets:wheelchair" expression=""/>
    <default applyOnUpdate="0" field="shelter" expression=""/>
    <default applyOnUpdate="0" field="addr:street" expression=""/>
    <default applyOnUpdate="0" field="addr:city" expression=""/>
    <default applyOnUpdate="0" field="delivery" expression=""/>
    <default applyOnUpdate="0" field="man_made" expression=""/>
    <default applyOnUpdate="0" field="usage" expression=""/>
    <default applyOnUpdate="0" field="tracks" expression=""/>
    <default applyOnUpdate="0" field="railway" expression=""/>
    <default applyOnUpdate="0" field="gauge" expression=""/>
    <default applyOnUpdate="0" field="electrified" expression=""/>
    <default applyOnUpdate="0" field="disused:railway" expression=""/>
    <default applyOnUpdate="0" field="mtb:scale:uphill" expression=""/>
    <default applyOnUpdate="0" field="note_2" expression=""/>
    <default applyOnUpdate="0" field="source:maxspeed:forward" expression=""/>
    <default applyOnUpdate="0" field="source:maxspeed:backward" expression=""/>
    <default applyOnUpdate="0" field="levelpart" expression=""/>
    <default applyOnUpdate="0" field="building:part" expression=""/>
    <default applyOnUpdate="0" field="mtb:scale:imba" expression=""/>
    <default applyOnUpdate="0" field="alt_name" expression=""/>
    <default applyOnUpdate="0" field="node" expression=""/>
    <default applyOnUpdate="0" field="destination:ref:forward" expression=""/>
    <default applyOnUpdate="0" field="is_sidepath" expression=""/>
    <default applyOnUpdate="0" field="abandoned:highway" expression=""/>
    <default applyOnUpdate="0" field="length" expression=""/>
    <default applyOnUpdate="0" field="public_transport" expression=""/>
    <default applyOnUpdate="0" field="maxwidth" expression=""/>
    <default applyOnUpdate="0" field="crossing:island" expression=""/>
    <default applyOnUpdate="0" field="crossing" expression=""/>
    <default applyOnUpdate="0" field="destination:lanes" expression=""/>
    <default applyOnUpdate="0" field="sulky" expression=""/>
    <default applyOnUpdate="0" field="cycleway:right:segregated" expression=""/>
    <default applyOnUpdate="0" field="cycleway:left:lane" expression=""/>
    <default applyOnUpdate="0" field="fixme_1" expression=""/>
    <default applyOnUpdate="0" field="sac_scale" expression=""/>
    <default applyOnUpdate="0" field="addr:postcode" expression=""/>
    <default applyOnUpdate="0" field="maxspeed:hgv" expression=""/>
    <default applyOnUpdate="0" field="maxspeed:caravan" expression=""/>
    <default applyOnUpdate="0" field="turn:lanes:forward" expression=""/>
    <default applyOnUpdate="0" field="check_date:tracktype" expression=""/>
    <default applyOnUpdate="0" field="maxweight:signed" expression=""/>
    <default applyOnUpdate="0" field="change:lanes" expression=""/>
    <default applyOnUpdate="0" field="amenity" expression=""/>
    <default applyOnUpdate="0" field="footway" expression=""/>
    <default applyOnUpdate="0" field="opening_date" expression=""/>
    <default applyOnUpdate="0" field="construction" expression=""/>
    <default applyOnUpdate="0" field="maxweight:forestry" expression=""/>
    <default applyOnUpdate="0" field="maxweight:agricultural" expression=""/>
    <default applyOnUpdate="0" field="sidewalk:right:bicycle" expression=""/>
    <default applyOnUpdate="0" field="loc_name" expression=""/>
    <default applyOnUpdate="0" field="direction" expression=""/>
    <default applyOnUpdate="0" field="material" expression=""/>
    <default applyOnUpdate="0" field="handrail:right" expression=""/>
    <default applyOnUpdate="0" field="surface:middle" expression=""/>
    <default applyOnUpdate="0" field="wikipedia" expression=""/>
    <default applyOnUpdate="0" field="wikidata" expression=""/>
    <default applyOnUpdate="0" field="tactile_paving" expression=""/>
    <default applyOnUpdate="0" field="handrail" expression=""/>
    <default applyOnUpdate="0" field="psv" expression=""/>
    <default applyOnUpdate="0" field="vehicle" expression=""/>
    <default applyOnUpdate="0" field="bicycle:backward" expression=""/>
    <default applyOnUpdate="0" field="parking:lane:left" expression=""/>
    <default applyOnUpdate="0" field="parking:condition:left" expression=""/>
    <default applyOnUpdate="0" field="alley" expression=""/>
    <default applyOnUpdate="0" field="name:etymology:wikidata" expression=""/>
    <default applyOnUpdate="0" field="overtaking" expression=""/>
    <default applyOnUpdate="0" field="destination:ref:backward" expression=""/>
    <default applyOnUpdate="0" field="image" expression=""/>
    <default applyOnUpdate="0" field="heritage:operator" expression=""/>
    <default applyOnUpdate="0" field="heritage" expression=""/>
    <default applyOnUpdate="0" field="bldam:criteria" expression=""/>
    <default applyOnUpdate="0" field="parking:lane:both" expression=""/>
    <default applyOnUpdate="0" field="ramp:bicycle" expression=""/>
    <default applyOnUpdate="0" field="ramp" expression=""/>
    <default applyOnUpdate="0" field="snowmobile" expression=""/>
    <default applyOnUpdate="0" field="ski" expression=""/>
    <default applyOnUpdate="0" field="noexit" expression=""/>
    <default applyOnUpdate="0" field="noname" expression=""/>
    <default applyOnUpdate="0" field="surface:note" expression=""/>
    <default applyOnUpdate="0" field="check_date:surface" expression=""/>
    <default applyOnUpdate="0" field="step_count" expression=""/>
    <default applyOnUpdate="0" field="start_date" expression=""/>
    <default applyOnUpdate="0" field="check_date" expression=""/>
    <default applyOnUpdate="0" field="destination:backward" expression=""/>
    <default applyOnUpdate="0" field="sidewalk:both:bicycle" expression=""/>
    <default applyOnUpdate="0" field="destination:symbol" expression=""/>
    <default applyOnUpdate="0" field="split_from" expression=""/>
    <default applyOnUpdate="0" field="bus" expression=""/>
    <default applyOnUpdate="0" field="tunnel" expression=""/>
    <default applyOnUpdate="0" field="indoor" expression=""/>
    <default applyOnUpdate="0" field="snowplowing" expression=""/>
    <default applyOnUpdate="0" field="motorcycle" expression=""/>
    <default applyOnUpdate="0" field="motorcar" expression=""/>
    <default applyOnUpdate="0" field="goods" expression=""/>
    <default applyOnUpdate="0" field="destination:lanes:backward" expression=""/>
    <default applyOnUpdate="0" field="hgv:maxweight" expression=""/>
    <default applyOnUpdate="0" field="destination:symbol:forward" expression=""/>
    <default applyOnUpdate="0" field="informal" expression=""/>
    <default applyOnUpdate="0" field="level" expression=""/>
    <default applyOnUpdate="0" field="turn:lanes:backward" expression=""/>
    <default applyOnUpdate="0" field="lanes:forward" expression=""/>
    <default applyOnUpdate="0" field="lanes:backward" expression=""/>
    <default applyOnUpdate="0" field="cycleway:both:segregated" expression=""/>
    <default applyOnUpdate="0" field="note:de" expression=""/>
    <default applyOnUpdate="0" field="maxweight:conditional" expression=""/>
    <default applyOnUpdate="0" field="zone:maxspeed" expression=""/>
    <default applyOnUpdate="0" field="source:width" expression=""/>
    <default applyOnUpdate="0" field="parking:lane:right" expression=""/>
    <default applyOnUpdate="0" field="parking:condition:right:time_interval" expression=""/>
    <default applyOnUpdate="0" field="parking:condition:right" expression=""/>
    <default applyOnUpdate="0" field="cycleway:right:surface" expression=""/>
    <default applyOnUpdate="0" field="cycleway:right:smoothness" expression=""/>
    <default applyOnUpdate="0" field="cycleway:left:surface" expression=""/>
    <default applyOnUpdate="0" field="cycleway:left:smoothness" expression=""/>
    <default applyOnUpdate="0" field="cycleway:both:lane" expression=""/>
    <default applyOnUpdate="0" field="traffic_calming" expression=""/>
    <default applyOnUpdate="0" field="traffic_sign" expression=""/>
    <default applyOnUpdate="0" field="stroller" expression=""/>
    <default applyOnUpdate="0" field="service" expression=""/>
    <default applyOnUpdate="0" field="mtb:scale" expression=""/>
    <default applyOnUpdate="0" field="incline" expression=""/>
    <default applyOnUpdate="0" field="covered" expression=""/>
    <default applyOnUpdate="0" field="bicycle_road" expression=""/>
    <default applyOnUpdate="0" field="old_name" expression=""/>
    <default applyOnUpdate="0" field="segregated" expression=""/>
    <default applyOnUpdate="0" field="horse" expression=""/>
    <default applyOnUpdate="0" field="surface:lanes" expression=""/>
    <default applyOnUpdate="0" field="unsigned_ref" expression=""/>
    <default applyOnUpdate="0" field="access" expression=""/>
    <default applyOnUpdate="0" field="maxweight" expression=""/>
    <default applyOnUpdate="0" field="zone:traffic" expression=""/>
    <default applyOnUpdate="0" field="old_ref" expression=""/>
    <default applyOnUpdate="0" field="wheelchair" expression=""/>
    <default applyOnUpdate="0" field="trail_visibility" expression=""/>
    <default applyOnUpdate="0" field="oneway:bicycle" expression=""/>
    <default applyOnUpdate="0" field="motor_vehicle" expression=""/>
    <default applyOnUpdate="0" field="width" expression=""/>
    <default applyOnUpdate="0" field="destination:colour" expression=""/>
    <default applyOnUpdate="0" field="destination" expression=""/>
    <default applyOnUpdate="0" field="toll:N3" expression=""/>
    <default applyOnUpdate="0" field="motorroad" expression=""/>
    <default applyOnUpdate="0" field="embankment" expression=""/>
    <default applyOnUpdate="0" field="cycleway" expression=""/>
    <default applyOnUpdate="0" field="lane_markings" expression=""/>
    <default applyOnUpdate="0" field="description" expression=""/>
    <default applyOnUpdate="0" field="sidewalk:left" expression=""/>
    <default applyOnUpdate="0" field="sidewalk:both" expression=""/>
    <default applyOnUpdate="0" field="priority_road" expression=""/>
    <default applyOnUpdate="0" field="cycleway:left" expression=""/>
    <default applyOnUpdate="0" field="turn:lanes" expression=""/>
    <default applyOnUpdate="0" field="placement" expression=""/>
    <default applyOnUpdate="0" field="maxspeed:type" expression=""/>
    <default applyOnUpdate="0" field="cycleway:both" expression=""/>
    <default applyOnUpdate="0" field="source:lit" expression=""/>
    <default applyOnUpdate="0" field="maxspeed:forward" expression=""/>
    <default applyOnUpdate="0" field="maxspeed:backward" expression=""/>
    <default applyOnUpdate="0" field="maxspeed:conditional" expression=""/>
    <default applyOnUpdate="0" field="tracktype" expression=""/>
    <default applyOnUpdate="0" field="disused:aeroway" expression=""/>
    <default applyOnUpdate="0" field="maxheight" expression=""/>
    <default applyOnUpdate="0" field="reg_name" expression=""/>
    <default applyOnUpdate="0" field="note:name" expression=""/>
    <default applyOnUpdate="0" field="layer" expression=""/>
    <default applyOnUpdate="0" field="int_ref" expression=""/>
    <default applyOnUpdate="0" field="bridge:name" expression=""/>
    <default applyOnUpdate="0" field="bridge" expression=""/>
    <default applyOnUpdate="0" field="source:maxspeed" expression=""/>
    <default applyOnUpdate="0" field="sidewalk" expression=""/>
    <default applyOnUpdate="0" field="postal_code" expression=""/>
    <default applyOnUpdate="0" field="lit" expression=""/>
    <default applyOnUpdate="0" field="sidewalk:right" expression=""/>
    <default applyOnUpdate="0" field="junction" expression=""/>
    <default applyOnUpdate="0" field="foot" expression=""/>
    <default applyOnUpdate="0" field="cycleway:right" expression=""/>
    <default applyOnUpdate="0" field="bicycle" expression=""/>
    <default applyOnUpdate="0" field="oneway" expression=""/>
    <default applyOnUpdate="0" field="destination:ref" expression=""/>
    <default applyOnUpdate="0" field="hgv" expression=""/>
    <default applyOnUpdate="0" field="surface" expression=""/>
    <default applyOnUpdate="0" field="smoothness" expression=""/>
    <default applyOnUpdate="0" field="ref" expression=""/>
    <default applyOnUpdate="0" field="name" expression=""/>
    <default applyOnUpdate="0" field="maxspeed" expression=""/>
    <default applyOnUpdate="0" field="lanes" expression=""/>
    <default applyOnUpdate="0" field="destination:ref:to:forward" expression=""/>
    <default applyOnUpdate="0" field="destination:forward" expression=""/>
    <default applyOnUpdate="0" field="destination:colour:forward" expression=""/>
  </defaults>
  <constraints>
    <constraint constraints="0" field="full_id" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="osm_id" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="osm_type" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="highway" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="maxspeed:practical" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="proposed" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="flood_prone" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="ford" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="destination:ref:lanes" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="destination:colour:backward" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="footway:surface" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="cycleway:surface" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="is_sidepath:of:name" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="is_sidepath:of" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="grade" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="toilets:wheelchair" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="shelter" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="addr:street" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="addr:city" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="delivery" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="man_made" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="usage" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="tracks" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="railway" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="gauge" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="electrified" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="disused:railway" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="mtb:scale:uphill" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="note_2" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="source:maxspeed:forward" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="source:maxspeed:backward" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="levelpart" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="building:part" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="mtb:scale:imba" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="alt_name" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="node" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="destination:ref:forward" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="is_sidepath" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="abandoned:highway" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="length" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="public_transport" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="maxwidth" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="crossing:island" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="crossing" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="destination:lanes" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="sulky" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="cycleway:right:segregated" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="cycleway:left:lane" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="fixme_1" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="sac_scale" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="addr:postcode" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="maxspeed:hgv" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="maxspeed:caravan" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="turn:lanes:forward" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="check_date:tracktype" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="maxweight:signed" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="change:lanes" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="amenity" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="footway" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="opening_date" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="construction" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="maxweight:forestry" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="maxweight:agricultural" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="sidewalk:right:bicycle" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="loc_name" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="direction" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="material" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="handrail:right" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="surface:middle" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="wikipedia" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="wikidata" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="tactile_paving" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="handrail" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="psv" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="vehicle" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="bicycle:backward" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="parking:lane:left" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="parking:condition:left" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="alley" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="name:etymology:wikidata" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="overtaking" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="destination:ref:backward" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="image" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="heritage:operator" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="heritage" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="bldam:criteria" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="parking:lane:both" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="ramp:bicycle" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="ramp" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="snowmobile" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="ski" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="noexit" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="noname" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="surface:note" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="check_date:surface" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="step_count" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="start_date" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="check_date" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="destination:backward" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="sidewalk:both:bicycle" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="destination:symbol" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="split_from" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="bus" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="tunnel" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="indoor" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="snowplowing" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="motorcycle" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="motorcar" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="goods" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="destination:lanes:backward" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="hgv:maxweight" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="destination:symbol:forward" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="informal" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="level" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="turn:lanes:backward" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="lanes:forward" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="lanes:backward" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="cycleway:both:segregated" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="note:de" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="maxweight:conditional" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="zone:maxspeed" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="source:width" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="parking:lane:right" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="parking:condition:right:time_interval" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="parking:condition:right" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="cycleway:right:surface" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="cycleway:right:smoothness" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="cycleway:left:surface" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="cycleway:left:smoothness" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="cycleway:both:lane" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="traffic_calming" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="traffic_sign" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="stroller" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="service" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="mtb:scale" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="incline" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="covered" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="bicycle_road" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="old_name" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="segregated" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="horse" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="surface:lanes" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="unsigned_ref" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="access" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="maxweight" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="zone:traffic" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="old_ref" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="wheelchair" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="trail_visibility" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="oneway:bicycle" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="motor_vehicle" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="width" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="destination:colour" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="destination" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="toll:N3" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="motorroad" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="embankment" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="cycleway" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="lane_markings" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="description" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="sidewalk:left" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="sidewalk:both" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="priority_road" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="cycleway:left" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="turn:lanes" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="placement" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="maxspeed:type" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="cycleway:both" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="source:lit" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="maxspeed:forward" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="maxspeed:backward" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="maxspeed:conditional" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="tracktype" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="disused:aeroway" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="maxheight" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="reg_name" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="note:name" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="layer" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="int_ref" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="bridge:name" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="bridge" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="source:maxspeed" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="sidewalk" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="postal_code" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="lit" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="sidewalk:right" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="junction" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="foot" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="cycleway:right" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="bicycle" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="oneway" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="destination:ref" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="hgv" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="surface" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="smoothness" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="ref" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="name" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="maxspeed" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="lanes" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="destination:ref:to:forward" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="destination:forward" notnull_strength="0" exp_strength="0" unique_strength="0"/>
    <constraint constraints="0" field="destination:colour:forward" notnull_strength="0" exp_strength="0" unique_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="full_id" desc=""/>
    <constraint exp="" field="osm_id" desc=""/>
    <constraint exp="" field="osm_type" desc=""/>
    <constraint exp="" field="highway" desc=""/>
    <constraint exp="" field="maxspeed:practical" desc=""/>
    <constraint exp="" field="proposed" desc=""/>
    <constraint exp="" field="flood_prone" desc=""/>
    <constraint exp="" field="ford" desc=""/>
    <constraint exp="" field="destination:ref:lanes" desc=""/>
    <constraint exp="" field="destination:colour:backward" desc=""/>
    <constraint exp="" field="footway:surface" desc=""/>
    <constraint exp="" field="cycleway:surface" desc=""/>
    <constraint exp="" field="is_sidepath:of:name" desc=""/>
    <constraint exp="" field="is_sidepath:of" desc=""/>
    <constraint exp="" field="grade" desc=""/>
    <constraint exp="" field="toilets:wheelchair" desc=""/>
    <constraint exp="" field="shelter" desc=""/>
    <constraint exp="" field="addr:street" desc=""/>
    <constraint exp="" field="addr:city" desc=""/>
    <constraint exp="" field="delivery" desc=""/>
    <constraint exp="" field="man_made" desc=""/>
    <constraint exp="" field="usage" desc=""/>
    <constraint exp="" field="tracks" desc=""/>
    <constraint exp="" field="railway" desc=""/>
    <constraint exp="" field="gauge" desc=""/>
    <constraint exp="" field="electrified" desc=""/>
    <constraint exp="" field="disused:railway" desc=""/>
    <constraint exp="" field="mtb:scale:uphill" desc=""/>
    <constraint exp="" field="note_2" desc=""/>
    <constraint exp="" field="source:maxspeed:forward" desc=""/>
    <constraint exp="" field="source:maxspeed:backward" desc=""/>
    <constraint exp="" field="levelpart" desc=""/>
    <constraint exp="" field="building:part" desc=""/>
    <constraint exp="" field="mtb:scale:imba" desc=""/>
    <constraint exp="" field="alt_name" desc=""/>
    <constraint exp="" field="node" desc=""/>
    <constraint exp="" field="destination:ref:forward" desc=""/>
    <constraint exp="" field="is_sidepath" desc=""/>
    <constraint exp="" field="abandoned:highway" desc=""/>
    <constraint exp="" field="length" desc=""/>
    <constraint exp="" field="public_transport" desc=""/>
    <constraint exp="" field="maxwidth" desc=""/>
    <constraint exp="" field="crossing:island" desc=""/>
    <constraint exp="" field="crossing" desc=""/>
    <constraint exp="" field="destination:lanes" desc=""/>
    <constraint exp="" field="sulky" desc=""/>
    <constraint exp="" field="cycleway:right:segregated" desc=""/>
    <constraint exp="" field="cycleway:left:lane" desc=""/>
    <constraint exp="" field="fixme_1" desc=""/>
    <constraint exp="" field="sac_scale" desc=""/>
    <constraint exp="" field="addr:postcode" desc=""/>
    <constraint exp="" field="maxspeed:hgv" desc=""/>
    <constraint exp="" field="maxspeed:caravan" desc=""/>
    <constraint exp="" field="turn:lanes:forward" desc=""/>
    <constraint exp="" field="check_date:tracktype" desc=""/>
    <constraint exp="" field="maxweight:signed" desc=""/>
    <constraint exp="" field="change:lanes" desc=""/>
    <constraint exp="" field="amenity" desc=""/>
    <constraint exp="" field="footway" desc=""/>
    <constraint exp="" field="opening_date" desc=""/>
    <constraint exp="" field="construction" desc=""/>
    <constraint exp="" field="maxweight:forestry" desc=""/>
    <constraint exp="" field="maxweight:agricultural" desc=""/>
    <constraint exp="" field="sidewalk:right:bicycle" desc=""/>
    <constraint exp="" field="loc_name" desc=""/>
    <constraint exp="" field="direction" desc=""/>
    <constraint exp="" field="material" desc=""/>
    <constraint exp="" field="handrail:right" desc=""/>
    <constraint exp="" field="surface:middle" desc=""/>
    <constraint exp="" field="wikipedia" desc=""/>
    <constraint exp="" field="wikidata" desc=""/>
    <constraint exp="" field="tactile_paving" desc=""/>
    <constraint exp="" field="handrail" desc=""/>
    <constraint exp="" field="psv" desc=""/>
    <constraint exp="" field="vehicle" desc=""/>
    <constraint exp="" field="bicycle:backward" desc=""/>
    <constraint exp="" field="parking:lane:left" desc=""/>
    <constraint exp="" field="parking:condition:left" desc=""/>
    <constraint exp="" field="alley" desc=""/>
    <constraint exp="" field="name:etymology:wikidata" desc=""/>
    <constraint exp="" field="overtaking" desc=""/>
    <constraint exp="" field="destination:ref:backward" desc=""/>
    <constraint exp="" field="image" desc=""/>
    <constraint exp="" field="heritage:operator" desc=""/>
    <constraint exp="" field="heritage" desc=""/>
    <constraint exp="" field="bldam:criteria" desc=""/>
    <constraint exp="" field="parking:lane:both" desc=""/>
    <constraint exp="" field="ramp:bicycle" desc=""/>
    <constraint exp="" field="ramp" desc=""/>
    <constraint exp="" field="snowmobile" desc=""/>
    <constraint exp="" field="ski" desc=""/>
    <constraint exp="" field="noexit" desc=""/>
    <constraint exp="" field="noname" desc=""/>
    <constraint exp="" field="surface:note" desc=""/>
    <constraint exp="" field="check_date:surface" desc=""/>
    <constraint exp="" field="step_count" desc=""/>
    <constraint exp="" field="start_date" desc=""/>
    <constraint exp="" field="check_date" desc=""/>
    <constraint exp="" field="destination:backward" desc=""/>
    <constraint exp="" field="sidewalk:both:bicycle" desc=""/>
    <constraint exp="" field="destination:symbol" desc=""/>
    <constraint exp="" field="split_from" desc=""/>
    <constraint exp="" field="bus" desc=""/>
    <constraint exp="" field="tunnel" desc=""/>
    <constraint exp="" field="indoor" desc=""/>
    <constraint exp="" field="snowplowing" desc=""/>
    <constraint exp="" field="motorcycle" desc=""/>
    <constraint exp="" field="motorcar" desc=""/>
    <constraint exp="" field="goods" desc=""/>
    <constraint exp="" field="destination:lanes:backward" desc=""/>
    <constraint exp="" field="hgv:maxweight" desc=""/>
    <constraint exp="" field="destination:symbol:forward" desc=""/>
    <constraint exp="" field="informal" desc=""/>
    <constraint exp="" field="level" desc=""/>
    <constraint exp="" field="turn:lanes:backward" desc=""/>
    <constraint exp="" field="lanes:forward" desc=""/>
    <constraint exp="" field="lanes:backward" desc=""/>
    <constraint exp="" field="cycleway:both:segregated" desc=""/>
    <constraint exp="" field="note:de" desc=""/>
    <constraint exp="" field="maxweight:conditional" desc=""/>
    <constraint exp="" field="zone:maxspeed" desc=""/>
    <constraint exp="" field="source:width" desc=""/>
    <constraint exp="" field="parking:lane:right" desc=""/>
    <constraint exp="" field="parking:condition:right:time_interval" desc=""/>
    <constraint exp="" field="parking:condition:right" desc=""/>
    <constraint exp="" field="cycleway:right:surface" desc=""/>
    <constraint exp="" field="cycleway:right:smoothness" desc=""/>
    <constraint exp="" field="cycleway:left:surface" desc=""/>
    <constraint exp="" field="cycleway:left:smoothness" desc=""/>
    <constraint exp="" field="cycleway:both:lane" desc=""/>
    <constraint exp="" field="traffic_calming" desc=""/>
    <constraint exp="" field="traffic_sign" desc=""/>
    <constraint exp="" field="stroller" desc=""/>
    <constraint exp="" field="service" desc=""/>
    <constraint exp="" field="mtb:scale" desc=""/>
    <constraint exp="" field="incline" desc=""/>
    <constraint exp="" field="covered" desc=""/>
    <constraint exp="" field="bicycle_road" desc=""/>
    <constraint exp="" field="old_name" desc=""/>
    <constraint exp="" field="segregated" desc=""/>
    <constraint exp="" field="horse" desc=""/>
    <constraint exp="" field="surface:lanes" desc=""/>
    <constraint exp="" field="unsigned_ref" desc=""/>
    <constraint exp="" field="access" desc=""/>
    <constraint exp="" field="maxweight" desc=""/>
    <constraint exp="" field="zone:traffic" desc=""/>
    <constraint exp="" field="old_ref" desc=""/>
    <constraint exp="" field="wheelchair" desc=""/>
    <constraint exp="" field="trail_visibility" desc=""/>
    <constraint exp="" field="oneway:bicycle" desc=""/>
    <constraint exp="" field="motor_vehicle" desc=""/>
    <constraint exp="" field="width" desc=""/>
    <constraint exp="" field="destination:colour" desc=""/>
    <constraint exp="" field="destination" desc=""/>
    <constraint exp="" field="toll:N3" desc=""/>
    <constraint exp="" field="motorroad" desc=""/>
    <constraint exp="" field="embankment" desc=""/>
    <constraint exp="" field="cycleway" desc=""/>
    <constraint exp="" field="lane_markings" desc=""/>
    <constraint exp="" field="description" desc=""/>
    <constraint exp="" field="sidewalk:left" desc=""/>
    <constraint exp="" field="sidewalk:both" desc=""/>
    <constraint exp="" field="priority_road" desc=""/>
    <constraint exp="" field="cycleway:left" desc=""/>
    <constraint exp="" field="turn:lanes" desc=""/>
    <constraint exp="" field="placement" desc=""/>
    <constraint exp="" field="maxspeed:type" desc=""/>
    <constraint exp="" field="cycleway:both" desc=""/>
    <constraint exp="" field="source:lit" desc=""/>
    <constraint exp="" field="maxspeed:forward" desc=""/>
    <constraint exp="" field="maxspeed:backward" desc=""/>
    <constraint exp="" field="maxspeed:conditional" desc=""/>
    <constraint exp="" field="tracktype" desc=""/>
    <constraint exp="" field="disused:aeroway" desc=""/>
    <constraint exp="" field="maxheight" desc=""/>
    <constraint exp="" field="reg_name" desc=""/>
    <constraint exp="" field="note:name" desc=""/>
    <constraint exp="" field="layer" desc=""/>
    <constraint exp="" field="int_ref" desc=""/>
    <constraint exp="" field="bridge:name" desc=""/>
    <constraint exp="" field="bridge" desc=""/>
    <constraint exp="" field="source:maxspeed" desc=""/>
    <constraint exp="" field="sidewalk" desc=""/>
    <constraint exp="" field="postal_code" desc=""/>
    <constraint exp="" field="lit" desc=""/>
    <constraint exp="" field="sidewalk:right" desc=""/>
    <constraint exp="" field="junction" desc=""/>
    <constraint exp="" field="foot" desc=""/>
    <constraint exp="" field="cycleway:right" desc=""/>
    <constraint exp="" field="bicycle" desc=""/>
    <constraint exp="" field="oneway" desc=""/>
    <constraint exp="" field="destination:ref" desc=""/>
    <constraint exp="" field="hgv" desc=""/>
    <constraint exp="" field="surface" desc=""/>
    <constraint exp="" field="smoothness" desc=""/>
    <constraint exp="" field="ref" desc=""/>
    <constraint exp="" field="name" desc=""/>
    <constraint exp="" field="maxspeed" desc=""/>
    <constraint exp="" field="lanes" desc=""/>
    <constraint exp="" field="destination:ref:to:forward" desc=""/>
    <constraint exp="" field="destination:forward" desc=""/>
    <constraint exp="" field="destination:colour:forward" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
    <actionsetting capture="0" id="{248aec21-8e38-4664-8122-9bd4f1c35e01}" name="OpenStreetMap Browser" type="5" isEnabledOnlyWhenEditable="0" shortTitle="OpenStreetMap Browser" notificationMessage="" action="http://www.openstreetmap.org/browse/[% &quot;osm_type&quot; %]/[% &quot;osm_id&quot; %]" icon="">
      <actionScope id="Feature"/>
      <actionScope id="Field"/>
      <actionScope id="Canvas"/>
    </actionsetting>
    <actionsetting capture="0" id="{7a859a26-a0f6-4445-bd18-05d62c12a05f}" name="JOSM" type="1" isEnabledOnlyWhenEditable="0" shortTitle="JOSM" notificationMessage="" action="from QuickOSM.core.actions import Actions;Actions.run(&quot;josm&quot;,&quot;[% &quot;full_id&quot; %]&quot;)" icon="/home/crischan/.local/share/QGIS/QGIS3/profiles/default/python/plugins/QuickOSM/resources/icons/josm_icon.svg">
      <actionScope id="Feature"/>
      <actionScope id="Field"/>
      <actionScope id="Canvas"/>
    </actionsetting>
    <actionsetting capture="0" id="{863a88ce-6083-4ca3-91f7-8f4c6b6a1727}" name="User default editor" type="5" isEnabledOnlyWhenEditable="0" shortTitle="User default editor" notificationMessage="" action="http://www.openstreetmap.org/edit?[% &quot;osm_type&quot; %]=[% &quot;osm_id&quot; %]" icon="">
      <actionScope id="Feature"/>
      <actionScope id="Field"/>
      <actionScope id="Canvas"/>
    </actionsetting>
    <actionsetting capture="0" id="{9e8bc1ab-f20a-4a9b-bb44-164ec95998ac}" name="Reload the query in a new file" type="1" isEnabledOnlyWhenEditable="0" shortTitle="Reload the query in a new file" notificationMessage="" action="from QuickOSM.core.actions import Actions;Actions.run_reload(layer_name=&quot;highway&quot;)" icon="">
      <actionScope id="Layer"/>
    </actionsetting>
  </attributeactions>
  <attributetableconfig sortOrder="0" actionWidgetStyle="dropDown" sortExpression="">
    <columns>
      <column width="-1" name="full_id" type="field" hidden="0"/>
      <column width="-1" name="osm_id" type="field" hidden="0"/>
      <column width="-1" name="osm_type" type="field" hidden="0"/>
      <column width="-1" name="highway" type="field" hidden="0"/>
      <column width="-1" name="oneway:bicycle" type="field" hidden="0"/>
      <column width="-1" name="informal" type="field" hidden="0"/>
      <column width="-1" name="ford" type="field" hidden="0"/>
      <column width="-1" name="overtaking" type="field" hidden="0"/>
      <column width="-1" name="snowplowing" type="field" hidden="0"/>
      <column width="-1" name="destination:colour:backward" type="field" hidden="0"/>
      <column width="-1" name="maxspeed:forward" type="field" hidden="0"/>
      <column width="-1" name="maxspeed:backward" type="field" hidden="0"/>
      <column width="-1" name="traffic_sign" type="field" hidden="0"/>
      <column width="-1" name="segregated" type="field" hidden="0"/>
      <column width="-1" name="maxheight" type="field" hidden="0"/>
      <column width="-1" name="surface:middle" type="field" hidden="0"/>
      <column width="-1" name="sac_scale" type="field" hidden="0"/>
      <column width="-1" name="abandoned:highway" type="field" hidden="0"/>
      <column width="-1" name="length" type="field" hidden="0"/>
      <column width="-1" name="motorcycle" type="field" hidden="0"/>
      <column width="-1" name="motorcar" type="field" hidden="0"/>
      <column width="-1" name="handrail" type="field" hidden="0"/>
      <column width="-1" name="vehicle" type="field" hidden="0"/>
      <column width="-1" name="check_date:tracktype" type="field" hidden="0"/>
      <column width="-1" name="destination:symbol" type="field" hidden="0"/>
      <column width="-1" name="bridge:name" type="field" hidden="0"/>
      <column width="-1" name="destination:lanes" type="field" hidden="0"/>
      <column width="-1" name="change:lanes" type="field" hidden="0"/>
      <column width="-1" name="incline" type="field" hidden="0"/>
      <column width="-1" name="sidewalk:both:bicycle" type="field" hidden="0"/>
      <column width="-1" name="tunnel" type="field" hidden="0"/>
      <column width="-1" name="junction" type="field" hidden="0"/>
      <column width="-1" name="old_name" type="field" hidden="0"/>
      <column width="-1" name="check_date:surface" type="field" hidden="0"/>
      <column width="-1" name="wheelchair" type="field" hidden="0"/>
      <column width="-1" name="trail_visibility" type="field" hidden="0"/>
      <column width="-1" name="snowmobile" type="field" hidden="0"/>
      <column width="-1" name="ski" type="field" hidden="0"/>
      <column width="-1" name="destination:colour" type="field" hidden="0"/>
      <column width="-1" name="destination" type="field" hidden="0"/>
      <column width="-1" name="destination:backward" type="field" hidden="0"/>
      <column width="-1" name="cycleway:right" type="field" hidden="0"/>
      <column width="-1" name="bicycle_road" type="field" hidden="0"/>
      <column width="-1" name="cycleway:both" type="field" hidden="0"/>
      <column width="-1" name="service" type="field" hidden="0"/>
      <column width="-1" name="surface:lanes" type="field" hidden="0"/>
      <column width="-1" name="horse" type="field" hidden="0"/>
      <column width="-1" name="width" type="field" hidden="0"/>
      <column width="-1" name="sidewalk:left" type="field" hidden="0"/>
      <column width="-1" name="foot" type="field" hidden="0"/>
      <column width="-1" name="cycleway:left" type="field" hidden="0"/>
      <column width="-1" name="noname" type="field" hidden="0"/>
      <column width="-1" name="description" type="field" hidden="0"/>
      <column width="-1" name="cycleway" type="field" hidden="0"/>
      <column width="-1" name="maxspeed:type" type="field" hidden="0"/>
      <column width="-1" name="access" type="field" hidden="0"/>
      <column width="-1" name="tracktype" type="field" hidden="0"/>
      <column width="-1" name="motor_vehicle" type="field" hidden="0"/>
      <column width="-1" name="bicycle" type="field" hidden="0"/>
      <column width="-1" name="layer" type="field" hidden="0"/>
      <column width="-1" name="bridge" type="field" hidden="0"/>
      <column width="-1" name="destination:symbol:forward" type="field" hidden="0"/>
      <column width="-1" name="destination:ref:to:forward" type="field" hidden="0"/>
      <column width="-1" name="sidewalk" type="field" hidden="0"/>
      <column width="-1" name="postal_code" type="field" hidden="0"/>
      <column width="-1" name="destination:forward" type="field" hidden="0"/>
      <column width="-1" name="destination:colour:forward" type="field" hidden="0"/>
      <column width="-1" name="lane_markings" type="field" hidden="0"/>
      <column width="-1" name="smoothness" type="field" hidden="0"/>
      <column width="-1" name="maxspeed:conditional" type="field" hidden="0"/>
      <column width="-1" name="destination:ref" type="field" hidden="0"/>
      <column width="-1" name="turn:lanes" type="field" hidden="0"/>
      <column width="-1" name="placement" type="field" hidden="0"/>
      <column width="-1" name="source:lit" type="field" hidden="0"/>
      <column width="-1" name="ref" type="field" hidden="0"/>
      <column width="-1" name="oneway" type="field" hidden="0"/>
      <column width="-1" name="maxspeed" type="field" hidden="0"/>
      <column width="-1" name="lit" type="field" hidden="0"/>
      <column width="-1" name="lanes" type="field" hidden="0"/>
      <column width="-1" name="int_ref" type="field" hidden="0"/>
      <column width="-1" name="surface" type="field" hidden="0"/>
      <column width="-1" name="name" type="field" hidden="0"/>
      <column width="-1" name="maxspeed:practical" type="field" hidden="0"/>
      <column width="-1" name="proposed" type="field" hidden="0"/>
      <column width="-1" name="flood_prone" type="field" hidden="0"/>
      <column width="-1" name="destination:ref:lanes" type="field" hidden="0"/>
      <column width="-1" name="footway:surface" type="field" hidden="0"/>
      <column width="-1" name="cycleway:surface" type="field" hidden="0"/>
      <column width="-1" name="is_sidepath:of:name" type="field" hidden="0"/>
      <column width="-1" name="is_sidepath:of" type="field" hidden="0"/>
      <column width="-1" name="grade" type="field" hidden="0"/>
      <column width="-1" name="toilets:wheelchair" type="field" hidden="0"/>
      <column width="-1" name="shelter" type="field" hidden="0"/>
      <column width="-1" name="addr:street" type="field" hidden="0"/>
      <column width="-1" name="addr:city" type="field" hidden="0"/>
      <column width="-1" name="delivery" type="field" hidden="0"/>
      <column width="-1" name="man_made" type="field" hidden="0"/>
      <column width="-1" name="usage" type="field" hidden="0"/>
      <column width="-1" name="tracks" type="field" hidden="0"/>
      <column width="-1" name="railway" type="field" hidden="0"/>
      <column width="-1" name="gauge" type="field" hidden="0"/>
      <column width="-1" name="electrified" type="field" hidden="0"/>
      <column width="-1" name="disused:railway" type="field" hidden="0"/>
      <column width="-1" name="mtb:scale:uphill" type="field" hidden="0"/>
      <column width="-1" name="note_2" type="field" hidden="0"/>
      <column width="-1" name="source:maxspeed:forward" type="field" hidden="0"/>
      <column width="-1" name="source:maxspeed:backward" type="field" hidden="0"/>
      <column width="-1" name="levelpart" type="field" hidden="0"/>
      <column width="-1" name="building:part" type="field" hidden="0"/>
      <column width="-1" name="mtb:scale:imba" type="field" hidden="0"/>
      <column width="-1" name="alt_name" type="field" hidden="0"/>
      <column width="-1" name="node" type="field" hidden="0"/>
      <column width="-1" name="destination:ref:forward" type="field" hidden="0"/>
      <column width="-1" name="is_sidepath" type="field" hidden="0"/>
      <column width="-1" name="public_transport" type="field" hidden="0"/>
      <column width="-1" name="maxwidth" type="field" hidden="0"/>
      <column width="-1" name="crossing:island" type="field" hidden="0"/>
      <column width="-1" name="crossing" type="field" hidden="0"/>
      <column width="-1" name="sulky" type="field" hidden="0"/>
      <column width="-1" name="cycleway:right:segregated" type="field" hidden="0"/>
      <column width="-1" name="cycleway:left:lane" type="field" hidden="0"/>
      <column width="-1" name="fixme_1" type="field" hidden="0"/>
      <column width="-1" name="addr:postcode" type="field" hidden="0"/>
      <column width="-1" name="maxspeed:hgv" type="field" hidden="0"/>
      <column width="-1" name="maxspeed:caravan" type="field" hidden="0"/>
      <column width="-1" name="turn:lanes:forward" type="field" hidden="0"/>
      <column width="-1" name="maxweight:signed" type="field" hidden="0"/>
      <column width="-1" name="amenity" type="field" hidden="0"/>
      <column width="-1" name="footway" type="field" hidden="0"/>
      <column width="-1" name="opening_date" type="field" hidden="0"/>
      <column width="-1" name="construction" type="field" hidden="0"/>
      <column width="-1" name="maxweight:forestry" type="field" hidden="0"/>
      <column width="-1" name="maxweight:agricultural" type="field" hidden="0"/>
      <column width="-1" name="sidewalk:right:bicycle" type="field" hidden="0"/>
      <column width="-1" name="loc_name" type="field" hidden="0"/>
      <column width="-1" name="direction" type="field" hidden="0"/>
      <column width="-1" name="material" type="field" hidden="0"/>
      <column width="-1" name="handrail:right" type="field" hidden="0"/>
      <column width="-1" name="wikipedia" type="field" hidden="0"/>
      <column width="-1" name="wikidata" type="field" hidden="0"/>
      <column width="-1" name="tactile_paving" type="field" hidden="0"/>
      <column width="-1" name="psv" type="field" hidden="0"/>
      <column width="-1" name="bicycle:backward" type="field" hidden="0"/>
      <column width="-1" name="parking:lane:left" type="field" hidden="0"/>
      <column width="-1" name="parking:condition:left" type="field" hidden="0"/>
      <column width="-1" name="alley" type="field" hidden="0"/>
      <column width="-1" name="name:etymology:wikidata" type="field" hidden="0"/>
      <column width="-1" name="destination:ref:backward" type="field" hidden="0"/>
      <column width="-1" name="image" type="field" hidden="0"/>
      <column width="-1" name="heritage:operator" type="field" hidden="0"/>
      <column width="-1" name="heritage" type="field" hidden="0"/>
      <column width="-1" name="bldam:criteria" type="field" hidden="0"/>
      <column width="-1" name="parking:lane:both" type="field" hidden="0"/>
      <column width="-1" name="ramp:bicycle" type="field" hidden="0"/>
      <column width="-1" name="ramp" type="field" hidden="0"/>
      <column width="-1" name="noexit" type="field" hidden="0"/>
      <column width="-1" name="surface:note" type="field" hidden="0"/>
      <column width="-1" name="step_count" type="field" hidden="0"/>
      <column width="-1" name="start_date" type="field" hidden="0"/>
      <column width="-1" name="check_date" type="field" hidden="0"/>
      <column width="-1" name="split_from" type="field" hidden="0"/>
      <column width="-1" name="bus" type="field" hidden="0"/>
      <column width="-1" name="indoor" type="field" hidden="0"/>
      <column width="-1" name="goods" type="field" hidden="0"/>
      <column width="-1" name="destination:lanes:backward" type="field" hidden="0"/>
      <column width="-1" name="hgv:maxweight" type="field" hidden="0"/>
      <column width="-1" name="level" type="field" hidden="0"/>
      <column width="-1" name="turn:lanes:backward" type="field" hidden="0"/>
      <column width="-1" name="lanes:forward" type="field" hidden="0"/>
      <column width="-1" name="lanes:backward" type="field" hidden="0"/>
      <column width="-1" name="cycleway:both:segregated" type="field" hidden="0"/>
      <column width="-1" name="note:de" type="field" hidden="0"/>
      <column width="-1" name="maxweight:conditional" type="field" hidden="0"/>
      <column width="-1" name="zone:maxspeed" type="field" hidden="0"/>
      <column width="-1" name="source:width" type="field" hidden="0"/>
      <column width="-1" name="parking:lane:right" type="field" hidden="0"/>
      <column width="-1" name="parking:condition:right:time_interval" type="field" hidden="0"/>
      <column width="-1" name="parking:condition:right" type="field" hidden="0"/>
      <column width="-1" name="cycleway:right:surface" type="field" hidden="0"/>
      <column width="-1" name="cycleway:right:smoothness" type="field" hidden="0"/>
      <column width="-1" name="cycleway:left:surface" type="field" hidden="0"/>
      <column width="-1" name="cycleway:left:smoothness" type="field" hidden="0"/>
      <column width="-1" name="cycleway:both:lane" type="field" hidden="0"/>
      <column width="-1" name="traffic_calming" type="field" hidden="0"/>
      <column width="-1" name="stroller" type="field" hidden="0"/>
      <column width="-1" name="mtb:scale" type="field" hidden="0"/>
      <column width="-1" name="covered" type="field" hidden="0"/>
      <column width="-1" name="unsigned_ref" type="field" hidden="0"/>
      <column width="-1" name="maxweight" type="field" hidden="0"/>
      <column width="-1" name="zone:traffic" type="field" hidden="0"/>
      <column width="-1" name="old_ref" type="field" hidden="0"/>
      <column width="-1" name="toll:N3" type="field" hidden="0"/>
      <column width="-1" name="motorroad" type="field" hidden="0"/>
      <column width="-1" name="embankment" type="field" hidden="0"/>
      <column width="-1" name="sidewalk:both" type="field" hidden="0"/>
      <column width="-1" name="priority_road" type="field" hidden="0"/>
      <column width="-1" name="disused:aeroway" type="field" hidden="0"/>
      <column width="-1" name="reg_name" type="field" hidden="0"/>
      <column width="-1" name="note:name" type="field" hidden="0"/>
      <column width="-1" name="source:maxspeed" type="field" hidden="0"/>
      <column width="-1" name="sidewalk:right" type="field" hidden="0"/>
      <column width="-1" name="hgv" type="field" hidden="0"/>
      <column width="-1" type="actions" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="abandoned:highway"/>
    <field editable="1" name="access"/>
    <field editable="1" name="addr:city"/>
    <field editable="1" name="addr:postcode"/>
    <field editable="1" name="addr:street"/>
    <field editable="1" name="alley"/>
    <field editable="1" name="alt_name"/>
    <field editable="1" name="amenity"/>
    <field editable="1" name="bicycle"/>
    <field editable="1" name="bicycle:backward"/>
    <field editable="1" name="bicycle_road"/>
    <field editable="1" name="bldam:criteria"/>
    <field editable="1" name="bridge"/>
    <field editable="1" name="bridge:name"/>
    <field editable="1" name="building:part"/>
    <field editable="1" name="bus"/>
    <field editable="1" name="change:lanes"/>
    <field editable="1" name="check_date"/>
    <field editable="1" name="check_date:surface"/>
    <field editable="1" name="check_date:tracktype"/>
    <field editable="1" name="construction"/>
    <field editable="1" name="covered"/>
    <field editable="1" name="crossing"/>
    <field editable="1" name="crossing:island"/>
    <field editable="1" name="cycleway"/>
    <field editable="1" name="cycleway:both"/>
    <field editable="1" name="cycleway:both:lane"/>
    <field editable="1" name="cycleway:both:segregated"/>
    <field editable="1" name="cycleway:left"/>
    <field editable="1" name="cycleway:left:lane"/>
    <field editable="1" name="cycleway:left:smoothness"/>
    <field editable="1" name="cycleway:left:surface"/>
    <field editable="1" name="cycleway:right"/>
    <field editable="1" name="cycleway:right:segregated"/>
    <field editable="1" name="cycleway:right:smoothness"/>
    <field editable="1" name="cycleway:right:surface"/>
    <field editable="1" name="cycleway:surface"/>
    <field editable="1" name="delivery"/>
    <field editable="1" name="description"/>
    <field editable="1" name="destination"/>
    <field editable="1" name="destination:backward"/>
    <field editable="1" name="destination:colour"/>
    <field editable="1" name="destination:colour:backward"/>
    <field editable="1" name="destination:colour:forward"/>
    <field editable="1" name="destination:forward"/>
    <field editable="1" name="destination:lanes"/>
    <field editable="1" name="destination:lanes:backward"/>
    <field editable="1" name="destination:ref"/>
    <field editable="1" name="destination:ref:backward"/>
    <field editable="1" name="destination:ref:forward"/>
    <field editable="1" name="destination:ref:lanes"/>
    <field editable="1" name="destination:ref:to:forward"/>
    <field editable="1" name="destination:symbol"/>
    <field editable="1" name="destination:symbol:forward"/>
    <field editable="1" name="direction"/>
    <field editable="1" name="disused:aeroway"/>
    <field editable="1" name="disused:railway"/>
    <field editable="1" name="electrified"/>
    <field editable="1" name="embankment"/>
    <field editable="1" name="fixme_1"/>
    <field editable="1" name="flood_prone"/>
    <field editable="1" name="foot"/>
    <field editable="1" name="footway"/>
    <field editable="1" name="footway:surface"/>
    <field editable="1" name="ford"/>
    <field editable="1" name="full_id"/>
    <field editable="1" name="gauge"/>
    <field editable="1" name="goods"/>
    <field editable="1" name="grade"/>
    <field editable="1" name="handrail"/>
    <field editable="1" name="handrail:right"/>
    <field editable="1" name="heritage"/>
    <field editable="1" name="heritage:operator"/>
    <field editable="1" name="hgv"/>
    <field editable="1" name="hgv:maxweight"/>
    <field editable="1" name="highway"/>
    <field editable="1" name="horse"/>
    <field editable="1" name="image"/>
    <field editable="1" name="incline"/>
    <field editable="1" name="indoor"/>
    <field editable="1" name="informal"/>
    <field editable="1" name="int_ref"/>
    <field editable="1" name="is_sidepath"/>
    <field editable="1" name="is_sidepath:of"/>
    <field editable="1" name="is_sidepath:of:name"/>
    <field editable="1" name="junction"/>
    <field editable="1" name="lane_markings"/>
    <field editable="1" name="lanes"/>
    <field editable="1" name="lanes:backward"/>
    <field editable="1" name="lanes:forward"/>
    <field editable="1" name="layer"/>
    <field editable="1" name="length"/>
    <field editable="1" name="level"/>
    <field editable="1" name="levelpart"/>
    <field editable="1" name="lit"/>
    <field editable="1" name="loc_name"/>
    <field editable="1" name="man_made"/>
    <field editable="1" name="material"/>
    <field editable="1" name="maxheight"/>
    <field editable="1" name="maxspeed"/>
    <field editable="1" name="maxspeed:backward"/>
    <field editable="1" name="maxspeed:caravan"/>
    <field editable="1" name="maxspeed:conditional"/>
    <field editable="1" name="maxspeed:forward"/>
    <field editable="1" name="maxspeed:hgv"/>
    <field editable="1" name="maxspeed:practical"/>
    <field editable="1" name="maxspeed:type"/>
    <field editable="1" name="maxweight"/>
    <field editable="1" name="maxweight:agricultural"/>
    <field editable="1" name="maxweight:conditional"/>
    <field editable="1" name="maxweight:forestry"/>
    <field editable="1" name="maxweight:signed"/>
    <field editable="1" name="maxwidth"/>
    <field editable="1" name="motor_vehicle"/>
    <field editable="1" name="motorcar"/>
    <field editable="1" name="motorcycle"/>
    <field editable="1" name="motorroad"/>
    <field editable="1" name="mtb:scale"/>
    <field editable="1" name="mtb:scale:imba"/>
    <field editable="1" name="mtb:scale:uphill"/>
    <field editable="1" name="name"/>
    <field editable="1" name="name:etymology:wikidata"/>
    <field editable="1" name="node"/>
    <field editable="1" name="noexit"/>
    <field editable="1" name="noname"/>
    <field editable="1" name="note:de"/>
    <field editable="1" name="note:name"/>
    <field editable="1" name="note_2"/>
    <field editable="1" name="old_name"/>
    <field editable="1" name="old_ref"/>
    <field editable="1" name="oneway"/>
    <field editable="1" name="oneway:bicycle"/>
    <field editable="1" name="opening_date"/>
    <field editable="1" name="osm_id"/>
    <field editable="1" name="osm_type"/>
    <field editable="1" name="overtaking"/>
    <field editable="1" name="parking:condition:left"/>
    <field editable="1" name="parking:condition:right"/>
    <field editable="1" name="parking:condition:right:time_interval"/>
    <field editable="1" name="parking:lane:both"/>
    <field editable="1" name="parking:lane:left"/>
    <field editable="1" name="parking:lane:right"/>
    <field editable="1" name="placement"/>
    <field editable="1" name="postal_code"/>
    <field editable="1" name="priority_road"/>
    <field editable="1" name="proposed"/>
    <field editable="1" name="psv"/>
    <field editable="1" name="public_transport"/>
    <field editable="1" name="railway"/>
    <field editable="1" name="ramp"/>
    <field editable="1" name="ramp:bicycle"/>
    <field editable="1" name="ref"/>
    <field editable="1" name="reg_name"/>
    <field editable="1" name="sac_scale"/>
    <field editable="1" name="segregated"/>
    <field editable="1" name="service"/>
    <field editable="1" name="shelter"/>
    <field editable="1" name="sidewalk"/>
    <field editable="1" name="sidewalk:both"/>
    <field editable="1" name="sidewalk:both:bicycle"/>
    <field editable="1" name="sidewalk:left"/>
    <field editable="1" name="sidewalk:right"/>
    <field editable="1" name="sidewalk:right:bicycle"/>
    <field editable="1" name="ski"/>
    <field editable="1" name="smoothness"/>
    <field editable="1" name="snowmobile"/>
    <field editable="1" name="snowplowing"/>
    <field editable="1" name="source:lit"/>
    <field editable="1" name="source:maxspeed"/>
    <field editable="1" name="source:maxspeed:backward"/>
    <field editable="1" name="source:maxspeed:forward"/>
    <field editable="1" name="source:width"/>
    <field editable="1" name="split_from"/>
    <field editable="1" name="start_date"/>
    <field editable="1" name="step_count"/>
    <field editable="1" name="stroller"/>
    <field editable="1" name="sulky"/>
    <field editable="1" name="surface"/>
    <field editable="1" name="surface:lanes"/>
    <field editable="1" name="surface:middle"/>
    <field editable="1" name="surface:note"/>
    <field editable="1" name="tactile_paving"/>
    <field editable="1" name="toilets:wheelchair"/>
    <field editable="1" name="toll:N3"/>
    <field editable="1" name="tracks"/>
    <field editable="1" name="tracktype"/>
    <field editable="1" name="traffic_calming"/>
    <field editable="1" name="traffic_sign"/>
    <field editable="1" name="trail_visibility"/>
    <field editable="1" name="tunnel"/>
    <field editable="1" name="turn:lanes"/>
    <field editable="1" name="turn:lanes:backward"/>
    <field editable="1" name="turn:lanes:forward"/>
    <field editable="1" name="unsigned_ref"/>
    <field editable="1" name="usage"/>
    <field editable="1" name="vehicle"/>
    <field editable="1" name="wheelchair"/>
    <field editable="1" name="width"/>
    <field editable="1" name="wikidata"/>
    <field editable="1" name="wikipedia"/>
    <field editable="1" name="zone:maxspeed"/>
    <field editable="1" name="zone:traffic"/>
  </editable>
  <labelOnTop>
    <field name="abandoned:highway" labelOnTop="0"/>
    <field name="access" labelOnTop="0"/>
    <field name="addr:city" labelOnTop="0"/>
    <field name="addr:postcode" labelOnTop="0"/>
    <field name="addr:street" labelOnTop="0"/>
    <field name="alley" labelOnTop="0"/>
    <field name="alt_name" labelOnTop="0"/>
    <field name="amenity" labelOnTop="0"/>
    <field name="bicycle" labelOnTop="0"/>
    <field name="bicycle:backward" labelOnTop="0"/>
    <field name="bicycle_road" labelOnTop="0"/>
    <field name="bldam:criteria" labelOnTop="0"/>
    <field name="bridge" labelOnTop="0"/>
    <field name="bridge:name" labelOnTop="0"/>
    <field name="building:part" labelOnTop="0"/>
    <field name="bus" labelOnTop="0"/>
    <field name="change:lanes" labelOnTop="0"/>
    <field name="check_date" labelOnTop="0"/>
    <field name="check_date:surface" labelOnTop="0"/>
    <field name="check_date:tracktype" labelOnTop="0"/>
    <field name="construction" labelOnTop="0"/>
    <field name="covered" labelOnTop="0"/>
    <field name="crossing" labelOnTop="0"/>
    <field name="crossing:island" labelOnTop="0"/>
    <field name="cycleway" labelOnTop="0"/>
    <field name="cycleway:both" labelOnTop="0"/>
    <field name="cycleway:both:lane" labelOnTop="0"/>
    <field name="cycleway:both:segregated" labelOnTop="0"/>
    <field name="cycleway:left" labelOnTop="0"/>
    <field name="cycleway:left:lane" labelOnTop="0"/>
    <field name="cycleway:left:smoothness" labelOnTop="0"/>
    <field name="cycleway:left:surface" labelOnTop="0"/>
    <field name="cycleway:right" labelOnTop="0"/>
    <field name="cycleway:right:segregated" labelOnTop="0"/>
    <field name="cycleway:right:smoothness" labelOnTop="0"/>
    <field name="cycleway:right:surface" labelOnTop="0"/>
    <field name="cycleway:surface" labelOnTop="0"/>
    <field name="delivery" labelOnTop="0"/>
    <field name="description" labelOnTop="0"/>
    <field name="destination" labelOnTop="0"/>
    <field name="destination:backward" labelOnTop="0"/>
    <field name="destination:colour" labelOnTop="0"/>
    <field name="destination:colour:backward" labelOnTop="0"/>
    <field name="destination:colour:forward" labelOnTop="0"/>
    <field name="destination:forward" labelOnTop="0"/>
    <field name="destination:lanes" labelOnTop="0"/>
    <field name="destination:lanes:backward" labelOnTop="0"/>
    <field name="destination:ref" labelOnTop="0"/>
    <field name="destination:ref:backward" labelOnTop="0"/>
    <field name="destination:ref:forward" labelOnTop="0"/>
    <field name="destination:ref:lanes" labelOnTop="0"/>
    <field name="destination:ref:to:forward" labelOnTop="0"/>
    <field name="destination:symbol" labelOnTop="0"/>
    <field name="destination:symbol:forward" labelOnTop="0"/>
    <field name="direction" labelOnTop="0"/>
    <field name="disused:aeroway" labelOnTop="0"/>
    <field name="disused:railway" labelOnTop="0"/>
    <field name="electrified" labelOnTop="0"/>
    <field name="embankment" labelOnTop="0"/>
    <field name="fixme_1" labelOnTop="0"/>
    <field name="flood_prone" labelOnTop="0"/>
    <field name="foot" labelOnTop="0"/>
    <field name="footway" labelOnTop="0"/>
    <field name="footway:surface" labelOnTop="0"/>
    <field name="ford" labelOnTop="0"/>
    <field name="full_id" labelOnTop="0"/>
    <field name="gauge" labelOnTop="0"/>
    <field name="goods" labelOnTop="0"/>
    <field name="grade" labelOnTop="0"/>
    <field name="handrail" labelOnTop="0"/>
    <field name="handrail:right" labelOnTop="0"/>
    <field name="heritage" labelOnTop="0"/>
    <field name="heritage:operator" labelOnTop="0"/>
    <field name="hgv" labelOnTop="0"/>
    <field name="hgv:maxweight" labelOnTop="0"/>
    <field name="highway" labelOnTop="0"/>
    <field name="horse" labelOnTop="0"/>
    <field name="image" labelOnTop="0"/>
    <field name="incline" labelOnTop="0"/>
    <field name="indoor" labelOnTop="0"/>
    <field name="informal" labelOnTop="0"/>
    <field name="int_ref" labelOnTop="0"/>
    <field name="is_sidepath" labelOnTop="0"/>
    <field name="is_sidepath:of" labelOnTop="0"/>
    <field name="is_sidepath:of:name" labelOnTop="0"/>
    <field name="junction" labelOnTop="0"/>
    <field name="lane_markings" labelOnTop="0"/>
    <field name="lanes" labelOnTop="0"/>
    <field name="lanes:backward" labelOnTop="0"/>
    <field name="lanes:forward" labelOnTop="0"/>
    <field name="layer" labelOnTop="0"/>
    <field name="length" labelOnTop="0"/>
    <field name="level" labelOnTop="0"/>
    <field name="levelpart" labelOnTop="0"/>
    <field name="lit" labelOnTop="0"/>
    <field name="loc_name" labelOnTop="0"/>
    <field name="man_made" labelOnTop="0"/>
    <field name="material" labelOnTop="0"/>
    <field name="maxheight" labelOnTop="0"/>
    <field name="maxspeed" labelOnTop="0"/>
    <field name="maxspeed:backward" labelOnTop="0"/>
    <field name="maxspeed:caravan" labelOnTop="0"/>
    <field name="maxspeed:conditional" labelOnTop="0"/>
    <field name="maxspeed:forward" labelOnTop="0"/>
    <field name="maxspeed:hgv" labelOnTop="0"/>
    <field name="maxspeed:practical" labelOnTop="0"/>
    <field name="maxspeed:type" labelOnTop="0"/>
    <field name="maxweight" labelOnTop="0"/>
    <field name="maxweight:agricultural" labelOnTop="0"/>
    <field name="maxweight:conditional" labelOnTop="0"/>
    <field name="maxweight:forestry" labelOnTop="0"/>
    <field name="maxweight:signed" labelOnTop="0"/>
    <field name="maxwidth" labelOnTop="0"/>
    <field name="motor_vehicle" labelOnTop="0"/>
    <field name="motorcar" labelOnTop="0"/>
    <field name="motorcycle" labelOnTop="0"/>
    <field name="motorroad" labelOnTop="0"/>
    <field name="mtb:scale" labelOnTop="0"/>
    <field name="mtb:scale:imba" labelOnTop="0"/>
    <field name="mtb:scale:uphill" labelOnTop="0"/>
    <field name="name" labelOnTop="0"/>
    <field name="name:etymology:wikidata" labelOnTop="0"/>
    <field name="node" labelOnTop="0"/>
    <field name="noexit" labelOnTop="0"/>
    <field name="noname" labelOnTop="0"/>
    <field name="note:de" labelOnTop="0"/>
    <field name="note:name" labelOnTop="0"/>
    <field name="note_2" labelOnTop="0"/>
    <field name="old_name" labelOnTop="0"/>
    <field name="old_ref" labelOnTop="0"/>
    <field name="oneway" labelOnTop="0"/>
    <field name="oneway:bicycle" labelOnTop="0"/>
    <field name="opening_date" labelOnTop="0"/>
    <field name="osm_id" labelOnTop="0"/>
    <field name="osm_type" labelOnTop="0"/>
    <field name="overtaking" labelOnTop="0"/>
    <field name="parking:condition:left" labelOnTop="0"/>
    <field name="parking:condition:right" labelOnTop="0"/>
    <field name="parking:condition:right:time_interval" labelOnTop="0"/>
    <field name="parking:lane:both" labelOnTop="0"/>
    <field name="parking:lane:left" labelOnTop="0"/>
    <field name="parking:lane:right" labelOnTop="0"/>
    <field name="placement" labelOnTop="0"/>
    <field name="postal_code" labelOnTop="0"/>
    <field name="priority_road" labelOnTop="0"/>
    <field name="proposed" labelOnTop="0"/>
    <field name="psv" labelOnTop="0"/>
    <field name="public_transport" labelOnTop="0"/>
    <field name="railway" labelOnTop="0"/>
    <field name="ramp" labelOnTop="0"/>
    <field name="ramp:bicycle" labelOnTop="0"/>
    <field name="ref" labelOnTop="0"/>
    <field name="reg_name" labelOnTop="0"/>
    <field name="sac_scale" labelOnTop="0"/>
    <field name="segregated" labelOnTop="0"/>
    <field name="service" labelOnTop="0"/>
    <field name="shelter" labelOnTop="0"/>
    <field name="sidewalk" labelOnTop="0"/>
    <field name="sidewalk:both" labelOnTop="0"/>
    <field name="sidewalk:both:bicycle" labelOnTop="0"/>
    <field name="sidewalk:left" labelOnTop="0"/>
    <field name="sidewalk:right" labelOnTop="0"/>
    <field name="sidewalk:right:bicycle" labelOnTop="0"/>
    <field name="ski" labelOnTop="0"/>
    <field name="smoothness" labelOnTop="0"/>
    <field name="snowmobile" labelOnTop="0"/>
    <field name="snowplowing" labelOnTop="0"/>
    <field name="source:lit" labelOnTop="0"/>
    <field name="source:maxspeed" labelOnTop="0"/>
    <field name="source:maxspeed:backward" labelOnTop="0"/>
    <field name="source:maxspeed:forward" labelOnTop="0"/>
    <field name="source:width" labelOnTop="0"/>
    <field name="split_from" labelOnTop="0"/>
    <field name="start_date" labelOnTop="0"/>
    <field name="step_count" labelOnTop="0"/>
    <field name="stroller" labelOnTop="0"/>
    <field name="sulky" labelOnTop="0"/>
    <field name="surface" labelOnTop="0"/>
    <field name="surface:lanes" labelOnTop="0"/>
    <field name="surface:middle" labelOnTop="0"/>
    <field name="surface:note" labelOnTop="0"/>
    <field name="tactile_paving" labelOnTop="0"/>
    <field name="toilets:wheelchair" labelOnTop="0"/>
    <field name="toll:N3" labelOnTop="0"/>
    <field name="tracks" labelOnTop="0"/>
    <field name="tracktype" labelOnTop="0"/>
    <field name="traffic_calming" labelOnTop="0"/>
    <field name="traffic_sign" labelOnTop="0"/>
    <field name="trail_visibility" labelOnTop="0"/>
    <field name="tunnel" labelOnTop="0"/>
    <field name="turn:lanes" labelOnTop="0"/>
    <field name="turn:lanes:backward" labelOnTop="0"/>
    <field name="turn:lanes:forward" labelOnTop="0"/>
    <field name="unsigned_ref" labelOnTop="0"/>
    <field name="usage" labelOnTop="0"/>
    <field name="vehicle" labelOnTop="0"/>
    <field name="wheelchair" labelOnTop="0"/>
    <field name="width" labelOnTop="0"/>
    <field name="wikidata" labelOnTop="0"/>
    <field name="wikipedia" labelOnTop="0"/>
    <field name="zone:maxspeed" labelOnTop="0"/>
    <field name="zone:traffic" labelOnTop="0"/>
  </labelOnTop>
  <reuseLastValue>
    <field name="abandoned:highway" reuseLastValue="0"/>
    <field name="access" reuseLastValue="0"/>
    <field name="addr:city" reuseLastValue="0"/>
    <field name="addr:postcode" reuseLastValue="0"/>
    <field name="addr:street" reuseLastValue="0"/>
    <field name="alley" reuseLastValue="0"/>
    <field name="alt_name" reuseLastValue="0"/>
    <field name="amenity" reuseLastValue="0"/>
    <field name="bicycle" reuseLastValue="0"/>
    <field name="bicycle:backward" reuseLastValue="0"/>
    <field name="bicycle_road" reuseLastValue="0"/>
    <field name="bldam:criteria" reuseLastValue="0"/>
    <field name="bridge" reuseLastValue="0"/>
    <field name="bridge:name" reuseLastValue="0"/>
    <field name="building:part" reuseLastValue="0"/>
    <field name="bus" reuseLastValue="0"/>
    <field name="change:lanes" reuseLastValue="0"/>
    <field name="check_date" reuseLastValue="0"/>
    <field name="check_date:surface" reuseLastValue="0"/>
    <field name="check_date:tracktype" reuseLastValue="0"/>
    <field name="construction" reuseLastValue="0"/>
    <field name="covered" reuseLastValue="0"/>
    <field name="crossing" reuseLastValue="0"/>
    <field name="crossing:island" reuseLastValue="0"/>
    <field name="cycleway" reuseLastValue="0"/>
    <field name="cycleway:both" reuseLastValue="0"/>
    <field name="cycleway:both:lane" reuseLastValue="0"/>
    <field name="cycleway:both:segregated" reuseLastValue="0"/>
    <field name="cycleway:left" reuseLastValue="0"/>
    <field name="cycleway:left:lane" reuseLastValue="0"/>
    <field name="cycleway:left:smoothness" reuseLastValue="0"/>
    <field name="cycleway:left:surface" reuseLastValue="0"/>
    <field name="cycleway:right" reuseLastValue="0"/>
    <field name="cycleway:right:segregated" reuseLastValue="0"/>
    <field name="cycleway:right:smoothness" reuseLastValue="0"/>
    <field name="cycleway:right:surface" reuseLastValue="0"/>
    <field name="cycleway:surface" reuseLastValue="0"/>
    <field name="delivery" reuseLastValue="0"/>
    <field name="description" reuseLastValue="0"/>
    <field name="destination" reuseLastValue="0"/>
    <field name="destination:backward" reuseLastValue="0"/>
    <field name="destination:colour" reuseLastValue="0"/>
    <field name="destination:colour:backward" reuseLastValue="0"/>
    <field name="destination:colour:forward" reuseLastValue="0"/>
    <field name="destination:forward" reuseLastValue="0"/>
    <field name="destination:lanes" reuseLastValue="0"/>
    <field name="destination:lanes:backward" reuseLastValue="0"/>
    <field name="destination:ref" reuseLastValue="0"/>
    <field name="destination:ref:backward" reuseLastValue="0"/>
    <field name="destination:ref:forward" reuseLastValue="0"/>
    <field name="destination:ref:lanes" reuseLastValue="0"/>
    <field name="destination:ref:to:forward" reuseLastValue="0"/>
    <field name="destination:symbol" reuseLastValue="0"/>
    <field name="destination:symbol:forward" reuseLastValue="0"/>
    <field name="direction" reuseLastValue="0"/>
    <field name="disused:aeroway" reuseLastValue="0"/>
    <field name="disused:railway" reuseLastValue="0"/>
    <field name="electrified" reuseLastValue="0"/>
    <field name="embankment" reuseLastValue="0"/>
    <field name="fixme_1" reuseLastValue="0"/>
    <field name="flood_prone" reuseLastValue="0"/>
    <field name="foot" reuseLastValue="0"/>
    <field name="footway" reuseLastValue="0"/>
    <field name="footway:surface" reuseLastValue="0"/>
    <field name="ford" reuseLastValue="0"/>
    <field name="full_id" reuseLastValue="0"/>
    <field name="gauge" reuseLastValue="0"/>
    <field name="goods" reuseLastValue="0"/>
    <field name="grade" reuseLastValue="0"/>
    <field name="handrail" reuseLastValue="0"/>
    <field name="handrail:right" reuseLastValue="0"/>
    <field name="heritage" reuseLastValue="0"/>
    <field name="heritage:operator" reuseLastValue="0"/>
    <field name="hgv" reuseLastValue="0"/>
    <field name="hgv:maxweight" reuseLastValue="0"/>
    <field name="highway" reuseLastValue="0"/>
    <field name="horse" reuseLastValue="0"/>
    <field name="image" reuseLastValue="0"/>
    <field name="incline" reuseLastValue="0"/>
    <field name="indoor" reuseLastValue="0"/>
    <field name="informal" reuseLastValue="0"/>
    <field name="int_ref" reuseLastValue="0"/>
    <field name="is_sidepath" reuseLastValue="0"/>
    <field name="is_sidepath:of" reuseLastValue="0"/>
    <field name="is_sidepath:of:name" reuseLastValue="0"/>
    <field name="junction" reuseLastValue="0"/>
    <field name="lane_markings" reuseLastValue="0"/>
    <field name="lanes" reuseLastValue="0"/>
    <field name="lanes:backward" reuseLastValue="0"/>
    <field name="lanes:forward" reuseLastValue="0"/>
    <field name="layer" reuseLastValue="0"/>
    <field name="length" reuseLastValue="0"/>
    <field name="level" reuseLastValue="0"/>
    <field name="levelpart" reuseLastValue="0"/>
    <field name="lit" reuseLastValue="0"/>
    <field name="loc_name" reuseLastValue="0"/>
    <field name="man_made" reuseLastValue="0"/>
    <field name="material" reuseLastValue="0"/>
    <field name="maxheight" reuseLastValue="0"/>
    <field name="maxspeed" reuseLastValue="0"/>
    <field name="maxspeed:backward" reuseLastValue="0"/>
    <field name="maxspeed:caravan" reuseLastValue="0"/>
    <field name="maxspeed:conditional" reuseLastValue="0"/>
    <field name="maxspeed:forward" reuseLastValue="0"/>
    <field name="maxspeed:hgv" reuseLastValue="0"/>
    <field name="maxspeed:practical" reuseLastValue="0"/>
    <field name="maxspeed:type" reuseLastValue="0"/>
    <field name="maxweight" reuseLastValue="0"/>
    <field name="maxweight:agricultural" reuseLastValue="0"/>
    <field name="maxweight:conditional" reuseLastValue="0"/>
    <field name="maxweight:forestry" reuseLastValue="0"/>
    <field name="maxweight:signed" reuseLastValue="0"/>
    <field name="maxwidth" reuseLastValue="0"/>
    <field name="motor_vehicle" reuseLastValue="0"/>
    <field name="motorcar" reuseLastValue="0"/>
    <field name="motorcycle" reuseLastValue="0"/>
    <field name="motorroad" reuseLastValue="0"/>
    <field name="mtb:scale" reuseLastValue="0"/>
    <field name="mtb:scale:imba" reuseLastValue="0"/>
    <field name="mtb:scale:uphill" reuseLastValue="0"/>
    <field name="name" reuseLastValue="0"/>
    <field name="name:etymology:wikidata" reuseLastValue="0"/>
    <field name="node" reuseLastValue="0"/>
    <field name="noexit" reuseLastValue="0"/>
    <field name="noname" reuseLastValue="0"/>
    <field name="note:de" reuseLastValue="0"/>
    <field name="note:name" reuseLastValue="0"/>
    <field name="note_2" reuseLastValue="0"/>
    <field name="old_name" reuseLastValue="0"/>
    <field name="old_ref" reuseLastValue="0"/>
    <field name="oneway" reuseLastValue="0"/>
    <field name="oneway:bicycle" reuseLastValue="0"/>
    <field name="opening_date" reuseLastValue="0"/>
    <field name="osm_id" reuseLastValue="0"/>
    <field name="osm_type" reuseLastValue="0"/>
    <field name="overtaking" reuseLastValue="0"/>
    <field name="parking:condition:left" reuseLastValue="0"/>
    <field name="parking:condition:right" reuseLastValue="0"/>
    <field name="parking:condition:right:time_interval" reuseLastValue="0"/>
    <field name="parking:lane:both" reuseLastValue="0"/>
    <field name="parking:lane:left" reuseLastValue="0"/>
    <field name="parking:lane:right" reuseLastValue="0"/>
    <field name="placement" reuseLastValue="0"/>
    <field name="postal_code" reuseLastValue="0"/>
    <field name="priority_road" reuseLastValue="0"/>
    <field name="proposed" reuseLastValue="0"/>
    <field name="psv" reuseLastValue="0"/>
    <field name="public_transport" reuseLastValue="0"/>
    <field name="railway" reuseLastValue="0"/>
    <field name="ramp" reuseLastValue="0"/>
    <field name="ramp:bicycle" reuseLastValue="0"/>
    <field name="ref" reuseLastValue="0"/>
    <field name="reg_name" reuseLastValue="0"/>
    <field name="sac_scale" reuseLastValue="0"/>
    <field name="segregated" reuseLastValue="0"/>
    <field name="service" reuseLastValue="0"/>
    <field name="shelter" reuseLastValue="0"/>
    <field name="sidewalk" reuseLastValue="0"/>
    <field name="sidewalk:both" reuseLastValue="0"/>
    <field name="sidewalk:both:bicycle" reuseLastValue="0"/>
    <field name="sidewalk:left" reuseLastValue="0"/>
    <field name="sidewalk:right" reuseLastValue="0"/>
    <field name="sidewalk:right:bicycle" reuseLastValue="0"/>
    <field name="ski" reuseLastValue="0"/>
    <field name="smoothness" reuseLastValue="0"/>
    <field name="snowmobile" reuseLastValue="0"/>
    <field name="snowplowing" reuseLastValue="0"/>
    <field name="source:lit" reuseLastValue="0"/>
    <field name="source:maxspeed" reuseLastValue="0"/>
    <field name="source:maxspeed:backward" reuseLastValue="0"/>
    <field name="source:maxspeed:forward" reuseLastValue="0"/>
    <field name="source:width" reuseLastValue="0"/>
    <field name="split_from" reuseLastValue="0"/>
    <field name="start_date" reuseLastValue="0"/>
    <field name="step_count" reuseLastValue="0"/>
    <field name="stroller" reuseLastValue="0"/>
    <field name="sulky" reuseLastValue="0"/>
    <field name="surface" reuseLastValue="0"/>
    <field name="surface:lanes" reuseLastValue="0"/>
    <field name="surface:middle" reuseLastValue="0"/>
    <field name="surface:note" reuseLastValue="0"/>
    <field name="tactile_paving" reuseLastValue="0"/>
    <field name="toilets:wheelchair" reuseLastValue="0"/>
    <field name="toll:N3" reuseLastValue="0"/>
    <field name="tracks" reuseLastValue="0"/>
    <field name="tracktype" reuseLastValue="0"/>
    <field name="traffic_calming" reuseLastValue="0"/>
    <field name="traffic_sign" reuseLastValue="0"/>
    <field name="trail_visibility" reuseLastValue="0"/>
    <field name="tunnel" reuseLastValue="0"/>
    <field name="turn:lanes" reuseLastValue="0"/>
    <field name="turn:lanes:backward" reuseLastValue="0"/>
    <field name="turn:lanes:forward" reuseLastValue="0"/>
    <field name="unsigned_ref" reuseLastValue="0"/>
    <field name="usage" reuseLastValue="0"/>
    <field name="vehicle" reuseLastValue="0"/>
    <field name="wheelchair" reuseLastValue="0"/>
    <field name="width" reuseLastValue="0"/>
    <field name="wikidata" reuseLastValue="0"/>
    <field name="wikipedia" reuseLastValue="0"/>
    <field name="zone:maxspeed" reuseLastValue="0"/>
    <field name="zone:traffic" reuseLastValue="0"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"bridge:name"</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
